%profile on
clear all;
close all;
addpath(genpath('../useful_functions'));
addpath(genpath('.'));

set_parameters_1; set_parameters_2;
n=pars1.n; m=n*n*n;
b=gen_right_side(pars1);
[f_obj,probl.fungrad]=set_f_obj(pars1,b);
probl.b=b; probl.n=n;
probl.optpar=options; probl.penparw=penparw;

for k=1:nsim
        x0=gen_x_start(pars1.x0_flag, pars1.varpaths, pars1.n);
        u0=1e-2*ones(2*m,1);   % Initiate Lagrange multipliers 
        stat0=compute_statistics1(x0,pars1);
        [x_opt,u,probl,res]=pbmsdp1(x0,u0,probl); % Call solver
        stat=compute_statistics1(x_opt,pars1);
end
clear u u0;

% profile viewer
