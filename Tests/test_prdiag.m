addpath(genpath('../Functions'))
n=5; k=1;
X=rand(n,n,n);
Y=rand(n,2*n-1);
c1=skal2d(prdiag(X,k) , Y);
c2=skal3d(X , prdiagad(Y,k));
diff=c1-c2;