addpath(genpath('Functions'));
eps=0.1;
h=0.001;
x=rand(1,1);
findiff=(penfun1(x+h,eps)-penfun1(x,eps))/h;
deriv=derivative_penfun1(x,eps);
diff=deriv-findiff;

deriv1=derivative_penfun1_base(shiftx(x),eps);
findiff1=(penfun1_base(shiftx(x+h),eps)-penfun1_base(shiftx(x),eps))/h;
diff1=deriv-findiff;
