clear all
addpath(genpath('../Functions'));
addpath(genpath('../../useful_functions'));
n=5;
h=0.00001;
xx=rand(n*n*n,1);

%optional parameters
%---------------------------------------------------
b=rand(3*n*n,1);
frame=false;
%---------------------------------------------------
f= @(x) f_resdiag(x,b,n,frame);
df=compute_grad_diag(xx,b,n,frame);
dfnum=derivative_num(f,xx,h);
diff=df-dfnum';
normdiff=norm(diff);
subplot(3,1,1); plot(df);
subplot(3,1,2); plot (dfnum);
subplot(3,1,3); plot (diff);

dfcube=reshape(df,[n,n,n]);
dfnumcube=reshape(dfnum,[n,n,n]);
diffcube=reshape(diff,[n,n,n]);


