%test
h=0.00001;
sum1=sum(phi);
fconstr_h=fconstr;
xh=x;
for i=1:nn
    xh(i)=x(i)+h;
    [phih,dphih,d2phih]= penfun(ff_constr(xh),penpar,u);
    sumh=sum(phih);
    grad_pensumh(i)=(sum1-sumh)/h;
    xh(i)=x(i);
end
diffh=grad_pensum-grad_pensumh; errh=norm(diffh);
%-----------------------------------------------------;