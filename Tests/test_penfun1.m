eps=0.8;
i=1;
x=[-1:0.01:2];
for i=1:length(x)
    y1(i)=derivative_penfun1(x(i),eps);
    y2(i)=penfun1(x(i),eps);
end
plot(x,y2);
grid on;