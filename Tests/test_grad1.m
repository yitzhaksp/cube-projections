clear all
addpath(genpath('../Functions'));
path;

n=3; m=n*n*n;
x=rand(n*n*n,1);
B=rand(3*n*n,1);
%y0=f_res(A0,B,n);
f= @(A) f_res1(A,B,n);

y=f(x);
xh=x;
h=0.000001;
grad_num=zeros(m,1);
for i=1:m
    xh(i)=x(i)+h; yh=f(xh); xh(i)=x(i);
    grad_num(i,1)=(yh-y)/h;
end

grad=compute_grad_op1(x,B,n);
diff_h=grad-grad_num; errh=norm(diff_h);
