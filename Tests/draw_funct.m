eps=0.01; c=1;
x=[-2:0.01:2];
entrybyentry=false;
%funct=@(x) sin(2*pi*(x-0.25))+1;
%funct=@(x) -fun1(x-0.5,eps)+0.5;
funct=@(x) penfun_bin2(x);
if (entrybyentry)
    for i=1:length(x)
        y(i)=funct(x(i));
    end
else
    y=funct(x);
end
plot(x,y);
grid on;