% mean( (x>barrier) )=mean(x)
function z=barrier(x)
a=100*(1-mean(x));
z=prctile(x,a);