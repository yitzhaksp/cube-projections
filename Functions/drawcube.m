function points=drawcube(A)
n=size(A,1);
ii=1;
for i=1:n
    disp(i); % to watch the speed
    for j=1:n
        for k=1:n
            if (A(i,j,k)==1)   
                points(ii,1)=i/n;  points(ii,2)=j/n; points(ii,3)=k/n;
                ii=ii+1;
            end
        end
    end   
end


