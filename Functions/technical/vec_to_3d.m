function A=vec_to_3d(A_vec,dims)
for i=1:dims(1)
    for j=1:dims(2)
        for k=1:dims(3)
            A(i,j,k) =A_vec(tvi(i,j,k,dims));
        end
    end
end

