% special rounding
function xbin = to_bin3( x )
a=100*(1-mean(x));
if(a>=0) && (a<=100)
    barrier=prctile(x,a);
    xbin=(x>barrier);
else
    disp('special rounding could not be performed ');
    xbin=x;
end

