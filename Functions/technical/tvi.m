% transform matrix index to vector index
function r=tvi(i,j,k,dims)
r=(i-1)*dims(2)*dims(3) + (j-1)*dims(3) + k;
