function xdit = to_bin( x )
m=length(x);
xdit=zeros(m,1);
a=rand(m,1);
for i=1:length(x)
    if a(i)<x(i)
        xdit(i)=1;
    else 
        xdit(i)=0;
    end
end
