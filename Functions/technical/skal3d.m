function y=skal3d(A,B)
y=sum(sum( squeeze(sum(A.*B,1)) ) );
end