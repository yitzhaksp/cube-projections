function A_vec= matr3d_to_vec(A,dims)
    for i=1:dims(1)
        for j=1:dims(2)
            for k=1:dims(3)    
                A_vec(  tvi(i,j,k,dims) , 1  )=A(i,j,k);
            end
        end
    end
   
 