function [A1,A2,A3]=get_proj(A_vec,n)
dimsA=[n,n,n];
A=reshape(A_vec,dimsA); 
A1=pr1(A); A2=pr2(A);  A3=pr3(A); 
