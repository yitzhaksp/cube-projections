function A=prdiagad(B,k)
n=size(B,1); m=size(B,2);
A=zeros(n,n,n);
if (    (m-(2*n-1)) ~=0 )
    error('wrong dim in prdiagad');
else
    for(ii=1:n)
        for i=1:ii
            j=ii+1-i;
            Aperm(i,j,:)=B(:,ii);
        end
    end

    for(ii=(n+1):(2*n-1))
        for i=(ii-n+1):n
            j=ii+1-i;
            Aperm(i,j,:)=B(:,ii);
        end
    end
    A=rotback(Aperm,k);

end