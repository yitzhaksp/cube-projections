function Arot=rotate(A,k)
switch k
    case 1
        Arot=A;
    case 2
        Arot=flipdim(A,2);
    case 3
        Arot=permute(A,[1,3,2]);
        Arot=flipdim(Arot,3);
end


