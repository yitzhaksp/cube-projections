function Y  = zerodim2( X )
n=size(X,1);
zeroo=zeros(n,n);
Y=X;
Y(:,1,:)=zeroo; Y(:,n,:)=zeroo;
end