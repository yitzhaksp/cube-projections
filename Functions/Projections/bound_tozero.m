function B=bound_tozero(A)
n=size(A,1);
B=A; zerovec=zeros(n,1);
B(1,:)=zerovec'; B(n,:)=zerovec'; B(:,1)=zerovec; B(:,n)=zerovec;