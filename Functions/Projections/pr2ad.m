function X=pr2ad(Y,n)
X=zeros(n,n,n);
for j=1:n
    X(:,j,:)=Y;
end