function X=pr3ad(Y,n)
X=zeros(n,n,n);
for k=1:n
    X(:,:,k)=Y;
end