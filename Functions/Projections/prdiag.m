function B=prdiag(A,k)
Aperm=rotate(A,k);
n=size(A,1);  
B=zeros(n,(2*n-1));
for(ii=1:n)
    for i=1:ii
        j=ii+1-i;
        B(:,ii)=B(:,ii)+squeeze(Aperm(i,j,:));
    end
end

for(ii=(n+1):(2*n-1))
    for i=(ii-n+1):n
        j=ii+1-i;
        B(:,ii)=B(:,ii)+squeeze(Aperm(i,j,:));
    end
end
