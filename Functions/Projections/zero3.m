function Y  = zero3( X )
n=size(X,1);
Y=X;
Y(1,:,:)=zeros(n,n); Y(n,:,:)=zeros(n,n);
Y(:,1,:)=zeros(n,n); Y(:,n,:)=zeros(n,n);
end