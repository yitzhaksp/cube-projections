function A=prdiagad_restr(B,k,frame)
n=size(B,1); m=size(B,2);
Atmp=prdiagad(B,k);
Atmp=rotate(Atmp,k);

if (    (m-(2*n-1)) ~=0 )
    error('wrong dim in prdiagad');
else
    nstart=compute_nstart(n);
    for ii=1:nstart-1
        for i=1:ii
            j=ii+1-i;
            Atmp(i,j,:)=0;
        end
    end

    for ii=(nstart+n):(2*n-1)
        for i=(ii-n+1):n
            j=ii+1-i;
            Atmp(i,j,:)=0;
        end
    end
    if (~frame)
        Atmp=zerodim3(Atmp);
    end
    A=rotback(Atmp,k);    
end


