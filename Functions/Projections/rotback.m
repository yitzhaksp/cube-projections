function Arotb=rotback(A,k)
    switch k
        case 1
            Arotb=A;
        case 2
            Arotb=flipdim(A,2);
        case 3
            Arotb=flipdim(A,3);
            Arotb=permute(Arotb,[1,3,2]);
    end