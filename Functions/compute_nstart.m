% for diagonal projection
function nstart=compute_nstart(n)
nstart=ceil(n/2);
