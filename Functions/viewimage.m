function y=viewimage(A,scale)
m=size(A,2);
for i=1:m
    if(scale==1)
        figure;imagesc(A{i});colormap(gray);colorbar;
    elseif(scale==2) 
        figure;imagesc(log(A{i}));colormap(gray);colorbar;
    end
end

function viewimage1(A,scale)
switch scale
    case 1
        subplot(221);imagesc(A{1});colormap(gray);colorbar
        subplot(222);imagesc(A{2});colormap(gray);colorbar
        subplot(223);imagesc(A{3});colormap(gray);colorbar
    case 2
        subplot(221);imagesc(log(A{1}));colormap(gray);colorbar
        subplot(222);imagesc(log(A{2}));colormap(gray);colorbar
        subplot(223);imagesc(log(A{3}));colormap(gray);colorbar
end