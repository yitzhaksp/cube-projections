function y=f_res1(A_vec,B_vec,n,pars)
dimsA=[n,n,n]; dimsB=[n,n,3]; m=n*n;
A=reshape(A_vec,dimsA); 

if (pars.angle=='orth')
    A1=pr1(A); A2=pr2(A); A3=pr3(A);
else
    A1=prdiag(A,1); A2=prdiag(A,2); A3=prdiag(A,3);
end
b1=B_vec(1:m,1); b2=B_vec((m+1):2*m,1); b3=B_vec((2*m+1):3*m);
B1=reshape(b1,n,n); B2=reshape(b2,n,n); B3=reshape(b3,n,n); 

if (pars.angle=='diag')
    nstart=compute_nstart(n);
    B1ext=zeros(n,2*n-1);  B1ext(:,nstart:(nstart+n-1))=B1;
    B2ext=zeros(n,2*n-1);  B2ext(:,nstart:(nstart+n-1))=B2;
    B3ext=zeros(n,2*n-1);  B3ext(:,nstart:(nstart+n-1))=B3;
    B1=B1ext;B2=B2ext; B3=B3ext;
end

C1=A1-B1; C2=A2-B2; C3=A3-B3;

if (pars.angle=='diag')
    C1=C1(:, nstart:(nstart+n-1)); 
    C2=C2(:, nstart:(nstart+n-1));
    C3=C3(:, nstart:(nstart+n-1));
end

if (~pars.frame)
    C1=C1(2:(n-1), 2:(n-1)); C2=C2(2:(n-1), 2:(n-1)); C3=C3(2:(n-1), 2:(n-1));
end
y=(norm (C1,'fro')^2 + norm( C2,'fro')^2  + norm(C3,'fro')^2);

