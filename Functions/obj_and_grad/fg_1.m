function [dphi,fobj,fconstr,F_aggr,grd_aggr,Hessaggr]=fg_1(x,u,penpar,probl,pars)
n=probl.n; nn=length(x);

h=probl.penparw.h;
c_bin=probl.penparw.c_bin; c_box=probl.penparw.c_box;
mid_bin=probl.penparw.mid_bin;
penbox_flag=probl.penparw.penbox_flag; % 0=penfun_box; 1=penfun
penbin_flag=probl.penparw.penbin_flag; % 0=sum, 1=x^2, 2=smoothened modul (wings down), 3=smooth modul (wings up) 
eps_bin=probl.penparw.eps_bin; a_bin=probl.penparw.a_bin;  mid_bin=probl.penparw.mid_bin;
a_box=probl.penparw.a_box;
scale_factor_obj=probl.penparw.scale_factor_obj;
scale_factor_pen=probl.penparw.scale_factor_pen;

fconstr=ff_constr(x);
[phi,dphi,d2phi]= penfun(fconstr,penpar,u); % penalty fun. and its derivatives
if (penbox_flag==0)
   penboxvec=penfun_box_a(x);
   penbox=sum(penboxvec);
   grad_penbox=derivative_num_vec(@penfun_box_a,x,h);   
elseif (penbox_flag==1) 
    penbox=sum(phi);
    %fprintf('penbox=%f\n',penbox);
    dphi1=dphi(1:nn); dphi2=dphi((nn+1):2*nn);
    grad_penbox=dphi2-dphi1;
end

% ux=u2.*x;
if (penbin_flag==0)
    penbin=sum(x);
    grad_penbin=ones(nn,1);
elseif (penbin_flag==1)
    penbin=dot( x, (ones(nn,1)-x) );
    grad_penbin=(ones(nn,1)-2*x);
elseif (penbin_flag==2) 
    penf_bin=@(x) penfun_bin1_a(x,eps_bin,mid_bin);
    penbinvec=penf_bin(x);
    penbin=sum(penbinvec);
    grad_penbin=derivative_num_vec(penf_bin,x,h); 
elseif (penbin_flag==3) 
    penf_bin=@(x) penfun_bin_a(x,eps_bin,a_bin);
    penbinvec=penf_bin(x);
    penbin=sum(penbinvec);
    grad_penbin=derivative_num_vec(penf_bin,x,h); 
   % penbinentry(i)=penfun_bin_a(x(i),eps_bin,a_bin);   
    %z=norm(grad_penbin);
end


fobj=f_res1(x, probl.b, n,pars); fobj=scale_factor_obj* fobj;
penalty=(c_box*penbox+c_bin*penbin); penalty=scale_factor_pen*penalty;
F_aggr= fobj+penalty;

if nargout>4,  %%%%%%%%%%%%% Compute gradient %%%%%%%%%%%%%
  grd_obj= compute_grad_op1(x,probl.b,n,pars); grd_obj=scale_factor_obj*grd_obj;
  grd_penalty= (c_box*grad_penbox+c_bin*grad_penbin); grd_penalty=scale_factor_pen*grd_penalty;
  grd_aggr=grd_obj+grd_penalty ;
  Hessaggr=0; % just for output var
  
  %z1=norm(grd_obj)
  %z2=norm(grad_pensum)
  %sz3=norm(grad_penbin)
  t=5; %meaningless
end