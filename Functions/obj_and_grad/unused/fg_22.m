function [dphi,fobj,fconstr,F_aggr,grd_aggr,Hess_aggr]=fg_22(x,u,penpar,probl)
% function, grad & Hess of penalty aggregate
% for nonnegative linear least squares
%
%  min 1/2 ||Ax-b||^2
%  s.t. x >= 0
%
%

% Michael Zibulevsky 21.10.2011
n=probl.n;
nn=length(x);
scale_factor=1/(nn);

%A=probl.A;
%b=probl.b;

%Ax=A*x;
%r=Ax-b;
%keyboard
fobj=f_res2(x, probl.b, n);
fconstr=ff_constr(x);
% u1=u(1:(2*nn),1); %Lagrange multipliers for box constraints
% u2=u((2*nn+1):(3*nn),1);  %Lagrange multipliers for binary constraints

[phi,dphi,d2phi]= penfun(fconstr,penpar,u); % penalty fun. and its derivatives
c_bin=0;
c_box=0;
penbin_flag=0;

% ux=u2.*x;
if (penbin_flag==0)
    penbin=sum(x);
    grad_penbin=ones(nn,1);
elseif (penbin_flag==1)
    penbin=dot( x, (ones(nn,1)-x) );
    grad_penbin=(ones(nn,1)-2*x);
elseif (penbin_flag==2)
    eps=0.01;
    for i=1:nn        
        penbinentry(i)=penfun1(x(i),eps);
        grad_penbin(i,1)=derivative_penfun1(x(i),eps);
    end
    penbin=sum(penbinentry);
    %z=norm(grad_penbin);
end

F_aggr= fobj+c_box*sum(phi)+c_bin*penbin;    

F_aggr= scale_factor * F_aggr;

if nargout>4,  %%%%%%%%%%%%% Compute gradient %%%%%%%%%%%%%

  grd_obj= compute_grad_op2(x,probl.b,n);
  dphi1=dphi(1:nn); dphi2=dphi((nn+1):2*nn);
  grad_pensum=dphi2-dphi1;
  grd_aggr=grd_obj + c_box*grad_pensum+c_bin*grad_penbin;
  %z1=norm(grd_obj)
  %z2=norm(grad_pensum)
  %sz3=norm(grad_penbin)
  grd_aggr= scale_factor * grd_aggr;

end
