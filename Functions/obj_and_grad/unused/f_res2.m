function y=f_res2(A_vec,B_vec,n)
dimsA=[n,n,n]; dimsB=[3,n,n];
A=reshape(A_vec,dimsA); B=reshape(B_vec,dimsB);

A1=squeeze(sum(A,1)); A1(1, :)=zeros(1,n); A1(n,:)=zeros(1,n); A1(:,1)=zeros(n,1); A1(:,n)=zeros(n,1);
A2=squeeze(sum(A,2)); A2(1,:)=zeros(1,n); A2(n,:)=zeros(1,n); A2(:,1)=zeros(n,1); A2(:,n)=zeros(n,1);
A3=squeeze(sum(A,3)); A3(1,:)=zeros(1,n); A3(n,:)=zeros(1,n); A3(:,1)=zeros(n,1); A3(:,n)=zeros(n,1);

B1=squeeze(B(1,:,:)); B1(1,:)=zeros(1,n); B1(n,:)=zeros(1,n); B1(:,1)=zeros(n,1); B1(:,n)=zeros(n,1);
B2=squeeze(B(2,:,:)); B2(1,:)=zeros(1,n); B2(n,:)=zeros(1,n); B2(:,1)=zeros(n,1); B2(:,n)=zeros(n,1);
B3=squeeze(B(3,:,:)); B3(1,:)=zeros(1,n); B3(n,:)=zeros(1,n); B3(:,1)=zeros(n,1); B3(:,n)=zeros(n,1);

y=( sum(sum(abs(A1-B1))) + sum(sum(abs(A2-B2)))  + sum(sum(abs(A3-B3)))  )/ (3*n*n);






