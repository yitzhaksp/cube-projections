function grad_vec=compute_grad1(A_vec,B_vec,n)
dimsA=[n,n,n]; dimsB=[3,n,n];
A=vec_to_3d(A_vec,dimsA); B=vec_to_3d(B_vec,dimsB);

A1=squeeze(sum(A,1));
A2=squeeze(sum(A,2));
A3=squeeze(sum(A,3));

B1=squeeze(B(1,:,:));
B2=squeeze(B(2,:,:));
B3=squeeze(B(3,:,:));

for i=1:n
    for j=1:n
        for k=1:n    
            grad_vec(  tvi(i,j,k,dimsA)  )=2*( (A1(j,k)-B1(j,k))+(A2(i,k)-B2(i,k))+(A3(i,j)-B3(i,j)) );
        end
    end
end





    
    
    
    



