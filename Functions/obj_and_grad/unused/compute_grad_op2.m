function grad_vec=compute_grad_op2(A_vec,B_vec,n)
dimsA=[n,n,n]; dimsB=[3,n,n];
A=reshape(A_vec,dimsA); B=reshape(B_vec,dimsB);

A1=pr1(A);  A2=pr2(A); A3=pr3(A);
B1=squeeze(B(1,:,:)); B2=squeeze(B(2,:,:)); B3=squeeze(B(3,:,:));

for i=1:n
    for j=1:n
            for k=1:n    
                grad(i,j,k)=( sign(A1(j,k)-B1(j,k)) + sign(A2(i,k)-B2(i,k)) + sign(A3(i,j)-B3(i,j)) ) / (3*n*n);
            end
    end
end
grad_vec=reshape(grad,length(A_vec),1);