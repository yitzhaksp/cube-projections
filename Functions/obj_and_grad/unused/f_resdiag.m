function y=f_resdiag(A_vec,B_vec,n,pars)
dimsA=[n,n,n]; m=n*n;
A=reshape(A_vec,dimsA); 
A1=prdiag(A,1); A2=prdiag(A,2); A3=prdiag(A,3);
b1=B_vec(1:m,1); b2=B_vec((m+1):2*m,1); b3=B_vec((2*m+1):3*m,1);
B1=reshape(b1,n,n); B2=reshape(b2,n,n); B3=reshape(b3,n,n);
nstart=compute_nstart(n);
B1ext=zeros(n,2*n-1);  B1ext(:,nstart:(nstart+n-1))=B1;
B2ext=zeros(n,2*n-1);  B2ext(:,nstart:(nstart+n-1))=B2;
B3ext=zeros(n,2*n-1);  B3ext(:,nstart:(nstart+n-1))=B3;

C1=A1-B1ext; C2=A2-B2ext; C3=A3-B3ext;
C1a=C1(:, nstart:(nstart+n-1)); 
C2a=C2(:, nstart:(nstart+n-1));
C3a=C3(:, nstart:(nstart+n-1));
if (~pars.frame)
    C1a=C1a(2:(n-1),:); 
    C2a=C2a(2:(n-1), :);
    C3a=C3a(2:(n-1), :);
end
y=(norm (C1a,'fro')^2 + norm( C2a,'fro')^2  + norm(C3a,'fro')^2);