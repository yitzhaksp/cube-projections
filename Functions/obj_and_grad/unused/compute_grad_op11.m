function grad_vec=compute_grad_op11(A_vec,B_vec,n)
dimsA=[n,n,n]; m=n*n; 
A=reshape(A_vec,dimsA); 
b1=B_vec(1:m,1); b2=B_vec((m+1):2*m,1); b3=B_vec((2*m+1):3*m,1);
B1=reshape(b1,n,n); B2=reshape(b2,n,n); B3=reshape(b3,n,n); 
grad=zeros(n,n,n);
grad=2*( zero1(pr1ad( (pr1(A)-B1),n )) )/(3*n*n);

%grad=2*( zero1(pr1ad( (pr1(A_hat)-B1),(n-1) )) + zero2(pr2ad((pr2(A_hat)-B2),(n-1))) + zero3(pr3ad(( pr3(A_hat)-B3),(n-1)) )/(3*(n-1)*(n-1)));
%grad_hat=2*( zero1(pr1ad( (pr1(A_hat)-B1),(n-1) )) + zero2(pr2ad(( pr2(A_hat)-B2),(n-1))) + zero3(pr3ad(( pr3(A_hat)-B3),(n-1)) )/(3*(n-1)*(n-1)));
%grad(1:(n-1) , 1:(n-1) , 1:(n-1))=grad_hat;
%grad(n,:,:)=zeros(n,n); grad(:,n,:)=zeros(n,n); grad(:,:,n)=zeros(n,n);
grad_vec=reshape(grad,length(A_vec),1);