function grad_vec=compute_grad_diag(A_vec,B_vec,n,frame)
dimsA=[n,n,n]; dimsB=[3,n,n]; m=n*n;
A=reshape(A_vec,dimsA); B=reshape(B_vec,dimsB);
b1=B_vec(1:m,1); b2=B_vec((m+1):2*m,1); b3=B_vec((2*m+1):3*m,1);
B1=reshape(b1,n,n); B2=reshape(b2,n,n); B3=reshape(b3,n,n);
nstart=compute_nstart(n);
B1ext=zeros(n,2*n-1);  B1ext(:,nstart:(nstart+n-1))=B1;
B2ext=zeros(n,2*n-1);  B2ext(:,nstart:(nstart+n-1))=B2;
B3ext=zeros(n,2*n-1);  B3ext(:,nstart:(nstart+n-1))=B3;
grad=zeros(n,n,n);
grad=2*(  prdiagad_restr(  (prdiag(A,1)-B1ext) , 1, frame)   + ...
                    prdiagad_restr(  (prdiag(A,2)-B2ext) , 2, frame) +  ...
                    prdiagad_restr(  (prdiag(A,3)-B3ext) , 3, frame) );
grad_vec=reshape(grad,length(A_vec),1);