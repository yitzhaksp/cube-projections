function y=f_res11(A_vec,B_vec,n)
dimsA=[n,n,n]; dimsB=[n,n,3]; m=n*n;
A=reshape(A_vec,dimsA); 

A1=pr1(A); A2=pr2(A); A3=pr3(A); 
b1=B_vec(1:m,1); b2=B_vec((m+1):2*m,1); b3=B_vec((2*m+1):3*m,1);
B1=reshape(b1,n,n); B2=reshape(b2,n,n); B3=reshape(b3,n,n); 

C1=A1-B1; C2=A2-B2; C3=A3-B3;
C1a=C1(2:(n-1), 2:(n-1)); C2a=C2(2:(n-1), 2:(n-1)); C3a=C3(2:(n-1), 2:(n-1));
y=(norm (C1a,'fro')^2)/ (3*n*n);
