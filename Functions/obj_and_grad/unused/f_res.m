function y=f_res(A_vec,B_vec,n)
dimsA=[n,n,n]; m=n*n;
A=reshape(A_vec,dimsA); 
A1=pr1(A); A2=pr2(A); A3=pr3(A);
b1=B_vec(1:m,1); b2=B_vec((m+1):2*m,1); b3=B_vec((2*m+1):3*m,1);
B1=reshape(b1,n,n); B2=reshape(b2,n,n); B3=reshape(b3,n,n);

y=(norm(A1-B1,'fro')^2 + norm(A2-B2,'fro')^2  + norm(A3-B3,'fro')^2)/ (3*n*n);






