function grad_vec=compute_grad_matr(A_vec,B_vec,n)
dimsA=[n,n,n]; dimsB=[3,n,n];
A=vec_to_3d(A_vec,dimsA); B=vec_to_3d(B_vec,dimsB);

A1=squeeze(sum(A,1));
A2=squeeze(sum(A,2));
A3=squeeze(sum(A,3));

B1=squeeze(B(1,:,:));
B2=squeeze(B(2,:,:));
B3=squeeze(B(3,:,:));

for i=1:n   
       grad( i,:,: )=2*( (A1-B1) + L_row(i,(A2-B2)) + (L_row(i,(A3-B3)))' )      
end 

grad_vec=matr3d_to_vec(grad,dimsA);