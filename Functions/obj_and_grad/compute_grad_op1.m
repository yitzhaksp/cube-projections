function grad_vec=compute_grad_op1(A_vec,B_vec,n,pars)
dimsA=[n,n,n]; m=n*n; ncb=n*n*n;
A=reshape(A_vec,dimsA);
b1=B_vec(1:m,1); b2=B_vec((m+1):2*m,1); b3=B_vec((2*m+1):3*m,1);
B1=reshape(b1,n,n); B2=reshape(b2,n,n); B3=reshape(b3,n,n);
grad=zeros(n,n,n);
C1=pr1(A)-B1;
C2=pr2(A)-B2;
C3=pr3(A)-B3;

C1ad=pr1ad( (pr1(A)-B1),n );
C2ad=pr2ad( (pr2(A)-B2),n );
C3ad=pr3ad( (pr3(A)-B3),n );

if (pars.frame) 
    grad=2*( C1ad + C2ad + C3ad );
else
    grad=2*( zero1(C1ad)+ zero2(C2ad) + zero3(C3ad ) );
end

if (pars.angle=='diag')
    nstart=compute_nstart(n);
    B1ext=zeros(n,2*n-1);  B1ext(:,nstart:(nstart+n-1))=B1;
    B2ext=zeros(n,2*n-1);  B2ext(:,nstart:(nstart+n-1))=B2;
    B3ext=zeros(n,2*n-1);  B3ext(:,nstart:(nstart+n-1))=B3;
    grad=2*(  prdiagad_restr(  (prdiag(A,1)-B1ext) , 1, frame)   + ...
                    prdiagad_restr(  (prdiag(A,2)-B2ext) , 2, frame) +  ...
                    prdiagad_restr(  (prdiag(A,3)-B3ext) , 3, frame) );
end

grad_vec=reshape(grad,length(A_vec),1);

