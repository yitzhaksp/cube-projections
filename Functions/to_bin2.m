function xbin = to_bin2( x )
m=length(x);
xbin=round(x);
a=rand(m,1);
for i=1:m
    if (xbin(i)<0)
        xbin(i)=0;
    elseif (xbin(i)>1)
        xbin(i)=1;
    end
end