function y = penfun_bin( x, eps,c )
if (x>0) && (x<1)
    y=-penfun1_base(shiftx(x),eps)+penfun1_base(shiftx(0),eps);
elseif (x>=1)
    y=c*(x-1);
else
    y=-c*x;
end