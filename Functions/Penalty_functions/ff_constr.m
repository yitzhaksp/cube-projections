function f_constr=ff_constr(x)
fconstr_bottom=-x;
fconstr_top=x-1;
f_constr=[ fconstr_bottom ; fconstr_top ];