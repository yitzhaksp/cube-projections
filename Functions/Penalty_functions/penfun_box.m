function y = penfun_box( x, c )
if (x>0) && (x<1)
    y=0;
elseif (x>=1)
    y=c*(x-1);
else
    y=-c*x;
end