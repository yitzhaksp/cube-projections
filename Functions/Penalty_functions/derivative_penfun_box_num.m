function df=derivative_penfun_box_num(x,h)
y=penfun_box_a(x); yh=penfun_box_a(x+h);
df=(yh-y)./h;