function y=derivative_penfun_box(x)
if (x>1)
    y=1;
elseif(x<0)
    y=-1;
else
    y=0;
end