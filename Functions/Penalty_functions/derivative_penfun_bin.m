function y=derivative_penfun_bin(x,eps,c)
if (x>1)
    y=c;
elseif (x<0)
    y=-c;
else
    y=-derivative_fun1(shiftx(x),eps);
end
