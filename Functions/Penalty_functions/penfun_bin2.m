function y = penfun_bin2( x)
y=-abs(x-0.5)+abs(-0.5)+2*penfun_box_a(x);