function df=derivative_penfun_bin_num(x,h,eps,c)
y=penfun_bin_a(x,eps,c); yh=penfun_bin_a(x+h,eps,c);
df=(yh-y)./h;

