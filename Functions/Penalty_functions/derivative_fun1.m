function y=derivative_fun1(x,eps)
c=( 2*abs(x)*(abs(x)+eps)-x^2 ) / (abs(x)+eps)^2 ;
if x>= 0
    y=c;
else
    y=-c;
end