function b=gen_right_side(pars1)
n=pars1.n;
dimsA=[n,n,n]; dimsB=[3,n,n]; dimsb1=[n*n,1];
if  (pars1.right_side_flag==1)
    pics=read_image(pars1.picpaths,pars1.n,pars1.img_maxvalue);
    B1=pics.B1; B2=pics.B2; B3=pics.B3;
else   
    if  (pars1.right_side_flag==2)
        A_vec=rand(n*n*n,1); A=vec_to_3d(A_vec,dimsA);    
    elseif (pars1.right_side_flag==3)
        pics=read_image(pars1.picpaths,pars1.n,pars1.img_maxvalue);
        A=zeros(n,n,n);
        A(1,:,:)=pics.B1; A(:,1,:)=pics.B2; A(:,:,1)=pics.B3;
    end
    B1=pr1(A); B2=pr2(A); B3=pr3(A);
end
b1=reshape(B1,dimsb1); b2=reshape(B2,dimsb1); b3=reshape(B3,dimsb1);
b=[b1;b2;b3]; 