function [err_box, err_bin]=compute_errors(x)
m=length(x);
a_box=penfun_box_a(x);
a_bin=penfun_bin2(x);
err_box=mean(a_box); %bin
err_bin=mean(a_bin); %box

