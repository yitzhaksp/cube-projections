options=pbm_optionset(); % Set standard parameters of  PBM algorithm

options.NouterTol = 10;   % max number of outer PBM iterations
options.N_ucTol   = 20;  % max number of iter. for_ unconstr.opt.
options.ucDXtol   = 1e-20; % accuracy of  unconstr.opt. in x
options.ucDFtol   = 1e-20; % accuracy unconstr.opt. in f
options.normGrdTol= 1e-8; % for_ Newton method

options.p_tol=1e-4;         % minimal penalty parameter
options.DFtol=1e-20;        % stopping crireria for PBM
options.INFEAStol=1e-20;    % change in the objectiive and infeasibility

%%%Unconstrained optimization tool to use :
%options.ucTool='newton_';  % For medium-size problems, when Hessian is available
%options.ucTool='minfunc';  % For medium and large problems: L-BFGS minFunc of Mark Schmidt, http://www.cs.ubc.ca/~schmidtm
options.ucTool ='LBFGS_C';   % L-BFGS in C by Naoaki Okazaki
%options.ucTool='fminunc';   % BFGS from matlab optimization toolbox
%options.ucTool='myfminu';   % old MATLAB toolbox unconstrained minimization (with Mzib modification)
%options.ucTool='sesop__';

options.NLP=1;    % 1 - PBM for NLP (nonlilnear programming)
options.SDP=0;    % 1 - PBM for SDP (nonlilnear semidefinite programming)
options.SUMMAX=0; % 1 - smoothing method of multipliers for summax problem

options.testgrad=0;           % 1 - test gradient, 0 - no
options.test_hess=0;           % 1 - test hessian, 0 - no
options.update_multiplires=1; % 1 - updeta multipliers, 0 - use just penalty function method

