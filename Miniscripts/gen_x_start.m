function x0=gen_x_start(x0_flag,paths,n)
m=n*n*n;
if (x0_flag==1)
    x0=rand(m,1);  % Initiate variable
elseif  (x0_flag==2)
    load(paths.x_start_path);
    x0=x_start(1:m,1);  % Initiate variables
elseif (x0_flag==3)
    x0=reshape(A,m,1);
elseif (x0_flag==4)
    load(paths.x_opt_path);
    x0=x_opt(1:m,1);
end