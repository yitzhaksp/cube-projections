probl.name= 'radon';
pars1.n=50;  
pars1.img_maxvalue=2;
pars1.picpaths.path1='data/imgs/originals/house.png';
pars1.picpaths.path2='data/imgs/originals/boat.png';
pars1.picpaths.path3='data/imgs/originals/barbara.png';
pars1.varpaths.x_start_path='data/x_start';
pars1.varpaths.x_opt_path='data/x_opt';
pars1.x0_flag=4; %1=rand; 2=xstart; 3=A; 4=x_opt
pars1.right_side_flag=1; % 1=img; 2= img->cube->proj
pars1.obj_fun_flag=1; %0=fres; 1=fres1 (fg1); 2=fres2 
pars1.frame=false; % true=include frame in target function
pars1.angle='orth'; % type of projection: 'orth' , 'diag'
penparw.scale_factor_obj=1/(3*pars1.n*pars1.n);
penparw.scale_factor_pen=1/(3*pars1.n*pars1.n);
penparw.c_box=1.0;
penparw.c_bin=0;
penparw.mid_bin=0.25;
penparw.penbox_flag=1; % 0=penfun_box; 1=penfun
penparw.penbin_flag=3; % 0=sum, 1=x^2, 2=smoothened modul (wings down), 3=smooth modul (wings up) 
penparw.eps_bin=0.01; penparw.a_bin=1;
penparw.a_box=1;
penparw.h=1e-4;
nsim=1;