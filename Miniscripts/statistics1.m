function [y,err_box,err_bin]= statistics1(x,fobj,n)
y=fobj(x)/(3*n*n);
[err_box, err_bin]=compute_errors(x);