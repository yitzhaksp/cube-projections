function [f_obj,fungrad]= set_f_obj(pars1,b)
if (pars1.obj_fun_flag==1)
    pars.frame=pars1.frame;
    pars.angle=pars1.angle;
    f_obj=@(x) f_res1(x,b,pars1.n,pars);
    fungrad = @(x,u,penpar,probl) fg_1(x,u,penpar,probl,pars);
elseif (pars1.obj_fun_flag==2)
    f_obj=@(x) f_res2(x,b,pars1.n);
    fungrad = @fg_22;
end