function pics=read_image(picpaths,n,img_maxvalue)
%'data/imgs/house.png'
B1=imread(picpaths.path1);
B2=imread(picpaths.path2);
B3=imread(picpaths.path3);
B1=double(B1);  B2=double(B2); B3=double(B3);%convert from integer to double
B1=imresize(B1,[n,n]); B2=imresize(B2,[n,n]); B3=imresize(B3,[n,n]);  %change image size
B1=img_maxvalue/max(B1(:))*B1;  B2=img_maxvalue/max(B2(:))*B2; B3=img_maxvalue/max(B3(:))*B3;% normalize amplitude
pics.B1=B1; pics.B2=B2; pics.B3=B3;

 
