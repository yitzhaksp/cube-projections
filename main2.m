clear all;
close all;
addpath(genpath('../useful_functions'));
addpath(genpath('.'));

set_parameters_1; set_parameters_2;
n=pars1.n; n2=n*n; n3=n*n*n;
load('data/picture1.dat');
load('data/picture2.dat');
load('data/picture3.dat');
load('data/Cube0.dat');
RightSide=[picture1;picture2;picture3];
[f_obj,probl.fungrad]=set_f_obj(pars1,RightSide);
probl.b=RightSide; probl.n=n;
probl.optpar=options; probl.penparw=penparw;
u0=1e-2*ones(2*m,1);   % Initiate Lagrange multipliers 
stat0=compute_statistics1(Cube0,pars1);
[x_opt,u,probl,res]=pbmsdp1(Cube0,u0,probl); % Call solver
stat=compute_statistics1(x_opt,pars1);

clear u u0;