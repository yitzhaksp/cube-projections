function [f,df,d2f]=phimax_lq(t,penpar,u,alpha,beta),
%
%Smooth quad-log approximation of max{alpha*t, beta*t}
%
%[f,df,d2f]=phimax_lq(t,u,penpar,alpha,beta)
%
%Input: 
%  t      - argument
%  penpar - penalty parameter: smoothing becomes sharp as p->0 
%  u      - Lagrange multiplier
%
%Output:
% f  - smoothing function
% df - first derivative;
% d2f - second derivative


c=1./penpar;

f=zeros(size(t));
df=zeros(size(t));
d2f=zeros(size(t));

  tgz=t>0;
  d= (~tgz).*alpha + tgz.*beta;

  tau=0.5*(d-u)./c;
  p=c.*tau.^2;
 
  tgtau=abs(t)>abs(tau);
  ind1=find(~tgtau);
  ind2=find(tgtau);
 
  t1=t(ind1);
  t2=t(ind2);
  u1=u(ind1);
  u2=u(ind2);

  d=d(ind2);
  tau=tau(ind2);
  p=p(ind2);


f(ind1)= 0.5*c.*t1.^2 + u1.*t1;
f(ind2)=d.*t2 - p.*log(t2./tau)+ 0.5*c.*tau.^2 + (u2-d).*tau;

if nargout >=2,
  df(ind1)=c.*t1 + u1;
  df(ind2)=d - p./t2;
end

if nargout >=3,
  d2f(ind1)=c;
  d2f(ind2)=p./t2.^2;
end

%keyboard
return

%%%%%%%% TEST

alpha=-2; beta=1; c=0.3; u= 0.3;
t=[-5:0.05:5];
f=phimax(t,4*c,u,alpha,beta);
ff=phimax(t,5*c,u,alpha,beta);

figure;plot(t,f,'--',t,u*t,'-.',t,max(alpha*t,beta*t),'-');grid
