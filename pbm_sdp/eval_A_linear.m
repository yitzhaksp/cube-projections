function AA = eval_A_linear(x,probl)

% A=probl.A0;
% k=size(probl.Ai,3);
% for i=1:k,
%     A=A+x(i)*probl.Ai(:,:,i);
% end


AA{1}=probl.A0 +mat(sparse(x')*probl.Ai);


