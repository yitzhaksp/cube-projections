function [f,df,d2f]=smooth_abs(t,p,u),

%function [f,df,d2f]=smooth_abs(t,p,u),
%smooth approximation of |t|; 
%become more sharp and accurate  when p->0

  [f,df,d2f] = phimax_lq(t,p,u,-1,1); return; % log-quadratic approximation to max-function

%  if any(abs(u)==1), keyboard;end

  tau=u./(1-abs(u));  

  z0 = abs(tau);

  %p=p./(1+z0).^2;   % Use penalty parameter dependent on tau: phi''(tau)=1;

  tt=(1./p).*t + tau;
  z = abs(tt);

  f0= p.*(z0-log(1+z0));
  f= p.*(z-log(1+z)) -f0;
  v=1./(1+z);
  %df=tt./(1+z);
  df=tt.*v;
  d2f= (v-z.*v.^2)./p;

  if any(abs(df)==1), keyboard;end
 
return






%%%%%%%%%%%%% Garbage (for test) %%%%%%%%%%


a=1;
da=0.001;
f=[-a:da:a]';
n=length(f);

u= -0.5*ones(n,1);
p=0.1;
[phi,dphi,d2phi]= smooth_abs(f,p,u);
%figure;plot(f,[phi,dphi]);grid
figure;plot(f,[phi,dphi,d2phi]);grid

figure;plot(f(1:n-1),[(phi(2:n)-phi(1:n-1))/da dphi(1:n-1)]);grid
figure;plot(f(1:n-1),[(dphi(2:n)-dphi(1:n-1))/da d2phi(1:n-1)]);grid

figure;plot(f(1:n-1),[d2phi(1:n-1)]);grid
figure;plot(f(1:n-1),[dphi(1:n-1)]);grid
figure;plot(f(1:n-1),(phi(2:n)-phi(1:n-1))/da);grid
figure;plot(f(1:n-1),(dphi(2:n)-dphi(1:n-1))/da);grid

