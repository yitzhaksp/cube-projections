%solve_nlsdp: solve nonlinear semidefinite programming problem  with PBM
%
% Michael Zibulevsky 11..2010

addpath(genpath('.'))
%addpath(genpath('C:\Users\mzibulevsky\Desktop\work-related\SESOP\SESOP_PACK\SesopTN2'))
%addpath(genpath('..\SESOP_PACK\SesopTN2'))

par.name= 'nlsdp';
par.fungrad = @fgh1_sdp; % User function for computing function, grad & Hess of
% the penalty aggregate
						 
INIT_DATA=0; % 1 - init data, 0- use old one from previous run

						  
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%     Generate data for quadratic programming problem
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

						  
% Create SDP matrices: A(x)= A0 +sum x_i Ai

m=5; %matrix size

n=3; % x - size


if  INIT_DATA
   
   if 1
	  
	  load ../sdp_matFormat/theta3.mat
	  par.A0=-mat(c);
	  par.Ai=A;
	  par.c=-b;
	  
   else
	  
	  A0=randn(m); A0= -A0'*A0; % random negative definite matrix
	  
	  par.A0=A0;
	  
	  for i=1:n,
		 tmp=randn(m); tmp=-tmp*tmp';
		 par.Ai(i,:) = tmp(:)';
	  end
	  
	  
	  par.c= rand(n,1);
	  
	  %save probl1 probl
   end
   
else 
   load probl_3_5
end

m=size(par.A0,1);
n=length(par.c);

par.eval_A = @eval_A_linear;
par.mult_adj_jacob_A= @mult_adj_jacob_A_linear;
par.mult_jacob_A= @mult_jacob_A_linear;


par.fg_obj=@fg_linear;
par.Z=eye(n); % computing HZ in fgh_sdp, we get th hessian 

%=========================================================================
%         SET  PARAMETERS  of  OPTIMIZATION ALGORITHM
%=========================================================================

options=pbm_optionset(); % Set standard parameters of  PBM algorithm

options.NouterTol = 30;    % max number of outer PBM iterations
options.N_ucTol   = 50;    % max number of iter. for_ unconstr.opt.
options.ucDXtol   = 1e-5;  % accuracy of  unconstr.opt. in x
options.ucDFtol   = 1e-7;  % accuracy unconstr.opt. in f
options.normGrdTol= 1e-5;  % for_ Newton method
options.Ngad_uncTol= 1;   % num of grad steps in frozen Hessian

options.update_multiplires=1; % 1 - updeta multipliers, 0 - use just penalty function method
options.mu_up=3 ;
options.mu_down= 1/options.mu_up;%0.3000;
options.u_tol= 1e-5;

options.penpar0= 0.1;
options.pFactor= 0.3;
options.p_tol=1e-6;

options.DFtol=1e-20;        % stopping crireria for PBM
options.INFEAStol=1e-20;    % change in the objectiive and infeasibility

%%%Unconstrained optimization tool to use :
%options.ucTool='newton_';   % For medium-size problems, when Hessian is available
options.ucTool='minfunc';  % For medium and large problems: L-BFGS minFunc of Mark Schmidt, http://www.cs.ubc.ca/~schmidtm
%options.ucTool='fminunc';   % BFGS from matlab optimization toolbox
%options.ucTool='myfminu';  % old MATLAB toolbox unconstrained minimization (with Mzib modification)
%options.ucTool='sesop__';

options.NLP=0;    % 1 - PBM for NLP (nonlilnear programming)
options.SDP=1;    % 1 - PBM for SDP (nonlilnear semidefinite programming)
options.SUMMAX=0; % 1 - smoothing method of multipliers for summax problem



par.optpar=options;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%      Solve the optimization problem with PBM method
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

x0=zeros(n,1);  % Initiate variables
x0=randn(n,1);

tmp=rand(m);
%u0.V{1}=tmp'*tmp;   % Initiate Lagrange multiplier matrix
u0.V{1}=eye(m);   % Initiate Lagrange multiplier matrix


[x,u,probl]=pbmsdp(x0,u0,probl); % Call solver

A_opt=par.eval_A(x,probl);
figure;subplot(121);plot(eig(A_opt{1}));grid; title('Eig(A)')
subplot(122);semilogy(eig(par.res.u.V{1}));grid; title('Eig(v)')







