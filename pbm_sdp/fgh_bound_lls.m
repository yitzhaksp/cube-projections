function [dphi,fobj,fconstr,F_aggr,grd_aggr,Hess_aggr]=fgh_bound_lls(x,u,penpar,probl)
% function, grad & Hess of penalty aggregate
% for nonnegative linear least squares
%
%  min 1/2 ||Ax-b||^2
%  s.t. x >= 0
%
%

% Michael Zibulevsky 21.10.2011


n=numel(x);
%scale_factor=1/n;
scale_factor=1;

A=probl.A;
b=probl.b;

Ax=A*x;
r=Ax-b;
%keyboard
fobj=0.5*r'*r;
fconstr=[probl.lb-x; x-probl.ub];

[phi,dphi,d2phi]= penfun(fconstr,penpar,u); % penalty fun. and its derivatives

F_aggr=fobj+sum(phi);

F_aggr= scale_factor * F_aggr;

if nargout>4,  %%%%%%%%%%%%% Compute gradient %%%%%%%%%%%%%

  grd_obj= (r'*A)';
  grd_aggr=grd_obj - dphi(1:n) + dphi((n+1):end);
  
    grd_aggr= scale_factor * grd_aggr;


end
if nargout>5,  %%%%%%%%%%%%% Compute Hessian %%%%%%%%%%%%%

 
  Hess_aggr=  A'*A + diag(d2phi(1:n)+d2phi((n+1):end));
  
   Hess_aggr= scale_factor * Hess_aggr;


end


