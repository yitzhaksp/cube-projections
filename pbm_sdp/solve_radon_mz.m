%profile on
clear all;
close all;
addpath(genpath('../Functions'));
addpath(genpath('.'));
n=150;  m=n*n*n;
read_image();
set_parameters_1; set_parameters_2;
set_f_obj;
dimsA=[n,n,n]; dimsB=[3,n,n]; dimsb1=[n*n,1];
A=zeros(n,n,n);
A(1,:,:)=B1; A(:,1,:)=B2; A(:,:,1)=B3;
gen_right_side;
probl.b=b; probl.n=n;

probl.optpar=options; probl.penparw=penparw;

for k=1:nsim
    gen_x_start;
    u0=3e-4*ones(2*m,1);   % Initiate Lagrange multipliers 
    y0=f_obj(x0,b,n);
    [x_opt0,u,probl]=pbmsdp1(x0,u0,probl); x_opt=x_opt0; % Call solver
    y_opt(k)=f_obj(x_opt,b,n);
    xbin1=to_bin(x_opt); ybin1=f_obj(xbin1,b,n);
    xbin2=to_bin2(x_opt); ybin2=f_obj(xbin2,b,n);
    compute_errors;
    y_opt=y_opt'; err_bin=err_bin'; err_box=err_box';
	 x_opt=x_opt0;
end

if (create_image_flag)
	%x_opt=x_opt0;ind=find(x_opt<0.1);x_opt(ind)=0;figure;hist(x_opt,100);x_opt=min(x_opt*100,1);
   view_image;
   %imwrite(A1_opt,'A1.jpg'); imwrite(A2_opt,'A2.jpg'); imwrite(A3_opt,'A3.jpg');
end

% profile viewer




