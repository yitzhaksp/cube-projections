function [x,f,g,B]=mybfgs(fg_name,x,optpar,varargin)
%
%BFGS Quasi-Newton unconstrained minimization.  M.Zibulevsky 
%
%[x,f,g,B]=mybfgs(fg_name,x,optpar,probl)
%
%Input:
%   fg_name - name of the user function, which provides 
%              function and gradient at given point x:
%       [f g]= funeval('fg_name',x,probl)
%       x       - vector of optimized variables
%       probl   - structure with user problem data
%       [f g ] - function and gradient at point x
%
%   x      - initial vector of optimized variables
%   optpar - parameters of the optimization method (can be empty [])
%   probl  - structure with user problem data, necessary for fg evaluation
%
%Output:
%   x - optimal point 
%   f - function value at x
%   g - gradient at x
%   B - approximation of inverse Hessian

if isfield(optpar,'normGrdTol')
  normGrdTol = optpar.normGrdTol; 
else
  normGrdTol = 1e-13;
end

if isfield(optpar,'NGrdTol')
  NgradTol   = optpar.NgradTol; 
else 
  NgradTol   = 5000;
end

if isfield(optpar,'B')
  B   = optpar.B; 
else 
  B=eye(n);  % Inv. Hessian approximation
end

n=length(x);

[f,g]= feval(fg_name,x,varargin{:}); % compute function & gradient at x
gold=g;
xold=x;

for iter=1:NgradTol,
  d=-B*g;  % Newton direction  

  %%%%%%%%%%%%%% Line search %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  b1=0;b2=1;EpsGold=0.1; dftol=1e-8; dxtol=1e-8; nlinsrchmax=30;
  %[xnew,fnew,gradnew,tbest,resEpsGold,b1,b2] = cublinsrch1(fg_name,...
  %  x,f,g,d,b1,b2,EpsGold,dftol,dxtol,nlinsrchmax,varargin{:});

  [xnew,fnew,gradnew]= armijostep(fg_name,x,f,g,d,0.3,0.3, varargin{:});

  x=xnew; f=fnew; g=gradnew;
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  
  
  [f,g]= feval(fg_name,x,varargin{:});
  
  normg=norm(gradnew);
  if normg<normGrdTol, break; end
  
  
  q=g-gold;
  p=x-xold;
  s=B*q;
  tau=s'*q; %tau=sign(tau+1e-20)*max(abs(tau),1e-8);
  mu=p'*q;  %mu=sign(mu+1e-20)*max(abs(mu),1e-8);

  v=p/mu-s/tau;
  
  B = B + p*p'/mu - s*s'/tau +  tau*v*v';   % BFGS update 
  
  
  gold=g;
  xold=x;
end












