function [invhess,directn]=updhess(xnew,xold,gradxnew,gradxold,invhess,para)
%UPDHESS Performs the Inverse Hessian Update.
%   Returns direction of search for use with 
%   unconstrained optimization problems. 

%   Copyright (c) 1990-98 by The MathWorks, Inc.
%   $Revision: 1.8 $  $Date: 1997/11/29 01:23:28 $
%   Andy Grace 7-9-90.

%  1-1-2000
%  Michael Zibulevsky change in order to achieve n^2 operations instead of n^3

u=xnew-xold;
v=gradxnew-gradxold;

para(1,6)=3; %%%%  M.Zibulevsky change in this line %%%%%%%

if para(1,6)==0
% The BFGS Hessian Update formula:
    invhess=invhess + v*v'/(v'*u)  -invhess*u*u'*invhess'/(u'*invhess*u);
    directn=-invhess\gradxnew;

elseif para(1,6)==1

% The DFP formula
    a=u*u'/(u'*v);
    b=-invhess*v*v'*invhess'/(v'*invhess*v);
    invhess=invhess + a + b;
    directn=-invhess*gradxnew;

elseif para(1,6)==3

% A formula given by Gill and Murray
    a = 1/(v'*u);

    %%% M.Zibulevsky changes: %%%%%%%%%%%%%%%%%%%%%%%
    %invhess=invhess - a*(invhess*v*u'+u*v'*invhess)+a*(1+v'*invhess*v*a)*u*u' ;
    b=invhess*v; C=b*u'; 
    invhess=invhess - a*( C+C' - (1+(v'*b)*a)*u*u') ;
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    directn=-invhess*gradxnew;
elseif para(1,6)==2
% Steepest Descent
    directn=-gradxnew;
end
       
