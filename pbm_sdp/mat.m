function a=mat(x);
n=sqrt(length(x(:)));
a=reshape(x,n,n);
