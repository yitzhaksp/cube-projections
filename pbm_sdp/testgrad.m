%function testgrad  % nested function

delta=1e-7; 
TEST_HESS=0;
rand('state',0);
%u0=u;
x0=x;

%u0= 1e-0*(rand(mconstr,1)+0.5);

%x0=rand(nx,1);

if TEST_HESS
  [dphi,f0,g0,H0]=fgh(x0,penpar,u0,probl);Hnum=zeros(size(H0));                
else
  [dphi,f0,g0]=fgh(x0,penpar,u0,probl);
end

grdnum=zeros(size(g0));

for i=1:nx,
  x=x0;
  x(i)=x(i)+delta;
  [dphi,f,g]=fgh(x,penpar,u0,probl);
  grdnum(i)=(f-f0)/delta;
  if TEST_HESS, Hnum(:,i)=(g-g0)/delta; end
end
x=x0;

figure; npl=3;
subplot(npl,1,1); plot(g0);grid; title('Analitical Gradient');
subplot(npl,1,2); plot(grdnum);grid; title('Numerical Gradient');
subplot(npl,1,3); plot(g0-grdnum);grid;title( 'Analitical - Numerical');
%subplot(npl,1,4); plot(u0);grid;

if TEST_HESS
 figure; npl=3;
 subplot(1,npl,1);imagesc(abs(H0));grid;colorbar; title('Analitical Hessian');
 subplot(1,npl,2);imagesc(abs(Hnum));grid;colorbar; title('Numerical Hessian');
 subplot(1,npl,3);imagesc(abs(H0-Hnum));grid;colorbar; title('Analitical- Numerical');

end
% H0
% Hnum
% H0byHnum=H0./Hnum
return



figure;plot([H0(:,13) Hnum(:,13)]);
%g0,grdnum-g0

