function x=cholsub(C,b,d)

%function x=cholsub(C,b)

% Solve C'Cx=b; C is upper triangular matrix;
%   or  C'diag(d)Cx=b

%C=triu(C);

y=C'\b;

if nargin>2, y=y./d;end
    
x=C\y;

return

H=rand(5);H=H*H'; 
b=rand(5,1) 
C=chol(H); 
x=cholsub(C,b);
Hx=H*x

[C,D]=mchol(H);
x=cholsub(C',b,diag(D));
Hx=H*x
