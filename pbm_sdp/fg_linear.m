function [f,g] =fg_linear(x,probl);


f=probl.c'*x;
g=probl.c;