function grd=grad_aggregate(x,penpar,multipliers,probl_data),
%Gradient of penalty aggregate

  [dphi,f,grd]=fgh(x,penpar,multipliers,probl_data);

