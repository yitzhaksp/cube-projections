function [x,u,par]=pbm(x0,u0,par),
% PBM - penalty/barrier multiplier algorithm for nonlinear optimization
%       with functional constraints:
%
%          min fobj(x)
%   subject to fconstr_i(x)<=0, i=1,..,nconstr
%
%
%[x,u,par]=pbm(x0,u0,par)
%
%INPUT:
%
%  x0    - initial vector of variables
%  u0    - initial vector of multipliers
%  par - structure with problem data and optimization parameters (see init_qp.m)
%
%OUTPUT:
%  x     - resulting vector of variables
%  u0    - resulting vector of multipliers
%  par - structure with problem data, optimization parameters and results


% Michael Zibulevsky,  08.04.2002;   24.08.2004; 09.02.2009        
%
%
% Copyright (c) 2002-2009. All rights reserved. Free for academic use. No warranty 


global N_FUN_EVAL N_GRAD_EVAL N_HESS_MULT N_HESS_EVAL GRAD_NORMS; %counters of function  & gradient calls
N_FUN_EVAL=0;N_GRAD_EVAL=0;N_HESS_MULT=0;N_HESS_EVAL=0; GRAD_NORMS=[];

CONTINUE_OLD=0;   % 1 - continue old run (load par. structure from file)
TEST_GRAD =0;     % 1 - test gradient, 0 - no

UPDATE_MULTIPLIERS=1; % 1 - updeta multipliers, 0 - use just penalty function method

%init_probl='init_tv';   % Name of user function to init problem data
%init_probl='init_adaptdeconv';   %
%init_probl='init_qp';   %
%init_probl= 'init_dualspars';
%init_probl= 'init_dualnew';

SUMMAX =0;         % 1 - smoothing method of multipliers for summax problem
mu_up=3;      %relative bounds on multipliers update;
mu_down=0.3; %
u_tol=1e-5;   % absolute lower bound on multipliers;

penpar0=0.7;     % initial penalty parameter
pFactor=0.5;
p_tol=2e-5;   % absolute lower bound on penalty parameter;

NouterTol=10000; % max number of outer iterations

N_ucTol=300;      % max number of iterations for unconstrained opt.
ucDXtol=1e-6;     % accuracy in x
ucDFtol=1e-6;     % accuracy in f
ucTool='minfunc'; % Optimization using L-BFGS: minFunc of Mark Schmidt

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%[par,x0,u0]=feval(init_probl);

setup_options();   % nested function

fname_res=[par.name '.res'];
funprintname= [par.name '_print'];


if(~CONTINUE_OLD),
	x=x0(:);
	u=u0(:);
	penpar=penpar0;
	tcpu0=cputime;
	iouter0=1;
	igad_unc=1e10;

	fobjs=[]; dfobjs=[];infeas=[]; gaps=[]; penpars=[];

	N_FUN_EVALs=0;
	N_GRAD_EVALs=0;
	N_HESS_EVALs=0;
	cputimes=0;
else,
	eval(['load ' fname_res ' -MAT']);
	x=par.res.x;
	u=par.res.u;
	penpar=par.res.penpar;
	tcpu0=cputime-par.res.cpu_time;
	N_FUN_EVALs=par.res.n_fun_evals
	N_GRAD_EVALs=par.res.n_grad_evals;
	N_HESS_EVALs=par.res.n_Hevals;
	cputimes=par.res.cputimes;
	dfobjs=par.res.dfobjs;
	infeas=par.res.infeas;
	gaps=par.res.gaps;
	penpars=par.res.penpars;
	%N_HESS_MULTs=par.res.n_Hmults;
	iouter0=par.res.iouter;
end


nx=length(x);
mconstr=length(u);

if TEST_GRAD, testgrad;return; end


if  1
	[dphi,fobj_old,fconstr,f_aggr]=feval(par.fungrad, x,u,penpar,par);
else
	[fobj_old,fconstr] = fc_problem(x,par);
end

if SUMMAX, fobj_old=fobj_old+norm(fconstr,1); end

if(~CONTINUE_OLD),
	dfobjs=fobj_old;
	infeas=max(fconstr);
	gaps= fobj_old-f_aggr;
	penpars=penpar;
end


fprintf('fobj=%g f_aggr=%g\n',fobj_old,f_aggr);



for iouter=iouter0:NouterTol, % ==================== OUTER PBM LOOP ============================

	minimize_aggregate(); % unconstrained minimization of penalty aggregate (nested function)

	if  1 % fungrad already includes penalty
		[dphi,fobj,fconstr,f_aggr,g_aggr]=feval(par.fungrad,x,u,penpar,par);
	else
		[fobj,fconstr] = fc_problem(x,par);
		[dphi,f_aggr,g_aggr]=fgh(x,penpar,u,par);
	end

	if SUMMAX,
		fobj=fobj+norm(fconstr,1);
		gap=fobj-f_aggr;
		gaps=[gaps;gap];
		dfobj=fobj-fobj_old;
		fprintf('\n******Out_iter #%d  penpar=%.2e normgrd=%.2e gap=%.4e dfobj=%.3e ****\n', ...
			iouter,penpar,norm(g_aggr),gap,dfobj);
		fprintf('fobj=%.12g f_aggr=%g Nfun=%g Ngrad=%g NH=%g NHmult=%g Cpu_time=%g\n',...
			fobj,f_aggr,N_FUN_EVAL,N_GRAD_EVAL, N_HESS_EVAL,N_HESS_MULT, cputime-tcpu0);

	else  %% we solve constrained optimization problem
		dfobj=fobj-fobj_old;
		fprintf('\n******Out_iter #%d  penpar=%.2e normgrd=%.2e infeas=%.4e dfobj=%.3e ****\n', ...
			iouter,penpar,norm(g_aggr),max(fconstr),dfobj);
		fprintf('fobj=%g f_aggr=%g Nfun=%g Ngrad=%g NH=%g NHmult=%g Cpu_time=%g\n',...
			fobj,f_aggr,N_FUN_EVAL,N_GRAD_EVAL, N_HESS_EVAL,N_HESS_MULT, cputime-tcpu0);

	end


	fobj_old=fobj;
	
	save_results();  % nested function


	%-----------------------------------------------------------
	%           Update multipliers and penalty parameter
	%-----------------------------------------------------------

	if UPDATE_MULTIPLIERS
	   if SUMMAX,
		  if(mu_up ~=1)
			 alpha= -1;
			 beta=1;
			 u1=min(mu_up*(u-alpha), dphi-alpha) +alpha;
			 u2=max(mu_down*(u-alpha),u1-alpha) +alpha;
			 u3 = beta - min(mu_up*(beta-u), beta-u2);  %beta - u3 = min(mu_up*(beta-u), beta-u2);
			 u4 = beta - max(mu_down*(beta-u), beta - u3);
			 u5=min(u4,beta-u_tol);
			 u=max(u5,alpha+u_tol);
		  end
	   else
		  u1=min(dphi,mu_up*u); u2=max(u1,mu_down*u); u=max(u2,u_tol);
	   end
	   
	end
	show_results(); % nested function
	
	if ~SUMMAX,
		if max(fconstr(:)) <= INFEAStol && abs(dfobj)<DFtol && abs(fobj-f_aggr)<DFtol, % stopping crireria for PBM constrained optimization
			disp('PBM: stopping criteria achieved');  return;
		end
	end

	penpar=max(penpar*pFactor,p_tol);

	
end % =========== end of outer loop ===============

return










%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%                              NESTED FUNCTIONS in pbmfun
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	%
	%                      setup_options()
	%
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

	function setup_options()

		if ~isempty(par.optpar.summax),    SUMMAX   = par.optpar.summax;end
		if ~isempty(par.optpar.mu_up),     mu_up    = par.optpar.mu_up;end
		if ~isempty(par.optpar.mu_down),   mu_down  = par.optpar.mu_down;end
		if ~isempty(par.optpar.u_tol),     u_tol    = par.optpar.u_tol;end
		if ~isempty(par.optpar.penpar0),   penpar0  = par.optpar.penpar0;end
		if ~isempty(par.optpar.pFactor),   pFactor  = par.optpar.pFactor;end
		if ~isempty(par.optpar.p_tol),     p_tol    = par.optpar.p_tol;end
		if ~isempty(par.optpar.NouterTol), NouterTol= par.optpar.NouterTol;end
		if ~isempty(par.optpar.N_ucTol),   N_ucTol  = par.optpar.N_ucTol;end
		if ~isempty(par.optpar.ucDXtol),   ucDXtol  = par.optpar.ucDXtol;end
		if ~isempty(par.optpar.ucDFtol),   ucDFtol  = par.optpar.ucDFtol;end
		if ~isempty(par.optpar.ucTool),    ucTool   = par.optpar.ucTool;end
		%if ~isempty(par.optpar.ucMethod),  ucMethod = par.optpar.ucMethod;end

		DFtol    = par.optpar.DFtol;        % stopping crireria for PBM
		INFEAStol= par.optpar.INFEAStol;    % change in the objectiive and infeasibility


		%if ~isempty(par.optpar.), = par.optpar.;end

	end








    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	%
	%   minimize_aggregate() - unconstrained minimization of the penalty aggregate
	%
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


	function minimize_aggregate()

		persistent old_dirs old_stps B_aggr;  % Memory on inverse Hessian for BFGS 

		%[x,dphi] = minimize_aggr(x,penpar,u,par)
		options=zeros(20,1); %options=foptions;
		options(1)=0;           % 1-display tabular results
		options(2)=ucDXtol;        % accuracy in x
		options(3)=ucDFtol;        % accuracy in f
		%options(4)=5;           % display frequency

		options(7)=1;          % 1-cubic linesearch
		%options(9)=1;         % 1-check user-supplied gradients
		options(14)=N_ucTol;   % Maximum number of function_ evaluations.


		if ucTool=='fminunc',        disp('NEW matlab optim.toolbox');
			new_options = optimset('GradObj','on','TolFun',ucDFtol,'TolX',ucDXtol,...
				'MaxFunEvals',N_ucTol,'LargeScale','on','Display','testing' );

			[x,fval,exitflag,output]= fminunc('fgh_aggregate',x,new_options,penpar,u,par);
			%[x,fval,exitflag,output]= fminunc_zib('fgh_aggregate',x,new_options,penpar,u,par);

		elseif ucTool=='minfunc',   disp('Optimization using L-BFGS: minFunc of Mark Schmidt')
			%options1 = optimset('GradObj','on','TolFun',ucDFtol,'TolX',ucDXtol,...
			%	'MaxFunEvals',N_ucTol,'LargeScale','on','Display','iter' );
			options1.Method = 'lbfgs';
			options1.TolFun=ucDFtol;
			options1.TolX=ucDXtol;
			options1.Display='iter';
			options1.Corr=8; % Number of previous steps used in L-BFGS update
			if iouter==iouter0
				old_dirs = zeros(nx,0);  % Memory of L-BFGS
				old_stps = zeros(nx,0);
			end

			%[x,fval,exitflag,output]= minFunc(@fgh_aggregate,x,options1,penpar,u,par);
			[x,fval, old_dirs, old_stps, exitflag,output]= minFunc_mz(@fgh_aggregate,x, old_dirs, old_stps, options1,penpar,u,par);


		elseif ucTool=='myfminu',     % old MATLAB toolbox unconstrained minimization (with Mzib modification)

			opti0ns(1)=3;          % Verbose
			options(5)=0;           % Algorithm strategy - BFGS
			if iouter==iouter0, B_aggr=eye(nx);end

			[x,options,B_aggr]=myfminu('fun_aggregate',x,options,'grad_aggregate',B_aggr,penpar,u,par);


		elseif ucTool=='mybfgs_',     % My BFGS (M.Zibulevsky)

			if iouter==iouter0, B_aggr=eye(nx);end
			%par.optpar.B=B_aggr; % Approximation of inverse Hessian for next call BFGS
			%[x,f_aggr,g_aggr,B_aggr]=mybfgs('fgh_aggregate',x,par.optpar,penpar,u,par);
			[x,f_aggr,g_aggr,B_aggr]=BFGS_ulbmz('fgh_aggregate',x,B_aggr,par.optpar, penpar,u,par);


		elseif ucTool=='newton_'

			%mynewton;
			newt_frozhess;

		elseif ucTool=='sesop__'
		   options=sesoptn_optionset;  % Get default options structure (see comments in optionset_sesoptn.m)
		   
		   options.sesop_figure_name=sprintf('SESOPtn  %d CG steps per TN iter',options.max_iter_CGinTN);
		   
		   options.nLastSteps=1;   % SESOP subspace will include: nLastSteps + [[precond] gradient]
		   options.AnalyticHessXmult=0;   % 1 Analytic multiplication by Hess in func_x; 0 - by finite differences
		   options.max_iter_CGinTN = 0;
		   options.max_sesop_iter  = 600;  % Max  SESOP iterations
		   options.max_newton_iter = 10;    % Max Newton iterations in subspace optimization
		   
		   options.dxTol = 1e-5;    % norm of change_in_x
		   options.gTol  = 1e-5;    % norm of the gradient
		   options.dfTol = 1e-6;   % abs change_in_objective_func
		   
		   
		   options.period_show_progress=20;    % Periodicity of showing sesop and user plots
		   
		   %options.report_func= @plot_results_fth_iaft;   % User function reference to display/collect iteration progress;
		   
		   %par.report_func=options.report_func;
		   
		   par.func_x=@fg_aggregate_sesop;
		   par.penpar=penpar;par.u=u;
		   [x,report]=sesoptn(x,[], par.func_x, [],[],options,par);

		   
		else
		   error('Unknown ucTool');
		end
		
		return
		
		
		
		
		
		
		
		%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
		%
		% newt_frozhess() - Newton method with frozen Hessian, nested in minimize_aggregate()
		%
		%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

		
		function newt_frozhess()

			persistent D ii CholFact;
			
			normGrdTol = par.optpar.normGrdTol; %1e=8;
			NnewtTol   = par.optpar.NnewtTol;     %40;
			Ngad_uncTol=par.optpar.Ngad_uncTol;
			CholTol=0;
			%Ngad_uncTol =10;

			Ngad_uncTol1=Ngad_uncTol;

			for inewt=1:NnewtTol,
				n=length(x);

				if igad_unc>=Ngad_uncTol1
					[f,g,H]= fgh_aggregate(x,penpar,u,par);

					if 0   %par.optpar.SPARSE_SOLVER
						fprintf('Sparse Cholessky factorization...')
						ii=par.indreord;
						CholFact=cholinc(H(ii,ii),CholTol);
					else
						%tic;fprintf('Cholessky factorization...')
						ii=[1:n];
						fprintf('\n');
						for ichol=1:20
							fprintf('chol%d ',ichol);
							[CholFact,p]=chol(H);
							if p<=0, break;end
							H=H+ 10^(2*ichol-4)*eye(n);
						end
						D=ones(n,1);

						% 			if p>0,
						% 				fprintf('Nonpos_Hess'); % eigvals=sort(eig(H))', H
						% 				%disp('Hessian is not positive definite. Modified Cholessky is used')
						% 				[CholFact,D]=mcholmz3(full(H)); CholFact=CholFact'; %Modified Colessky
						% 			end
					end

					%disp(' done'); toc

				end


				for igad_unc=1:Ngad_uncTol
					fprintf('.');
					[f,g]= fgh_aggregate(x,penpar,u,par);

					%fprintf('Cholessky substitution...');tic
					d=zeros(n,1);
					%d(ii)=-CholFact\g(ii);
					d(ii)= cholsub(CholFact,-g(ii),D);
					%disp(' done'); toc

					b1=0;b2=1;EpsGold=0.1; dftol=1e-8; dxtol=1e-8; nlinsrchmax=100;

					%[xnew,fnew,gradnew,tbest,resEpsGold,b1,b2] = cublinsrch1('fgh_aggregate',...
					%  x,f,g,d,b1,b2,EpsGold,dftol,dxtol,nlinsrchmax,  penpar,u,par);
					[xnew,fnew,gradnew]= armijostep('fgh_aggregate',x,f,g,d,0.3,0.3, penpar,u,par);

					x=xnew;
					normg=norm(gradnew);
					if normg<normGrdTol, break; end
				end
				if normg<normGrdTol, break; end
			end

		end % newt_frozhess

	end    %% minimize_aggregate


	
	
	
	
	
	

	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	%
	%                  show_results()
	%
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


	function show_results(); % nested function


		if rem(iouter,1)==0,
			%codex_print(x,u,penpar,par);
			feval(funprintname,x,u,penpar,par); % print/plot results
			mplots=1;
			nplots=2;
			figure(100);
			if SUMMAX
				subplot(mplots,nplots,1); semilogy(cputimes, abs((fobjs-(fobjs(end)))/max(fobjs)),'o',...
					cputimes, abs(gaps/max(fobjs)),'.');grid; title(' ')
				%subplot(mplots,nplots,1); semilogy(cputimes, gaps,'o');grid
				grid; title('abs((fobjs-(fobjs(end)))/max(abs(fobjs)))')
			else

				subplot(mplots,nplots,1); semilogy(cputimes, penpars,'r*', cputimes, abs(dfobjs)+1e-15,'b-',...
					cputimes, abs(infeas)+1e-15,'b--', cputimes, abs(gaps)+1e-15,'bo');grid;
				xlabel('CPU time, s'); axi=axis;axi(3:4)=[1e-15 1e5];axis(axi);
				title(sprintf('PBM iterations f_{obj}= %1.8e',fobj));
				legend('Penalty parameter','Change in obj.function', 'Constraint violation','Gap   |{obj} - {aggr}|')
				subplot(mplots,nplots,2); semilogy(GRAD_NORMS);title('Penalty Aggregate gradient norm');
				xlabel('Gradient evaluations')
			end
			drawnow;
		end

	end  % show_results
	
		
	
	

	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	%
	%                       save_results(); 
	%
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	 
	function save_results()

		dfobjs= [dfobjs;dfobj];
		infeas=[infeas;max(fconstr)];
		gaps=[gaps; (fobj-f_aggr)];
		penpars=[penpars;penpar];
		
		N_FUN_EVALs=[N_FUN_EVALs;N_FUN_EVAL];
		N_GRAD_EVALs=[N_GRAD_EVALs;N_GRAD_EVAL];
		N_HESS_EVALs=[N_HESS_EVALs;N_HESS_EVAL];
		cputimes=[cputimes;cputime-tcpu0];


		%fconstr
		%pbm_print; % print intermidiate results
		%% eval(par.print);
		%plot(u);grid;drawnow;


		par.res.x=x;
		par.res.u=u;
		par.res.penpar=penpar;
		par.res.infeas=max(fconstr);
		par.res.normgrd=norm(g_aggr);
		par.res.dfobj=fobj-fobj_old;
		par.res.f_aggr=f_aggr;
		par.res.fobj=fobj;
		par.res.iouter=iouter;
		par.res.cpu_time=cputime-tcpu0;
		par.res.n_fun_evals=N_FUN_EVALs;
		par.res.n_grad_evals=N_GRAD_EVALs;
		par.res.n_Hevals=N_HESS_EVALs;
		%par.res.n_Hmults=N_HESS_MULTs;
		par.res.cputimes=cputimes;
		par.res.dfobjs=dfobjs;
		par.res.infeas=infeas;
		par.res.gaps=gaps;
		par.res.penpars=penpars;

		if rem(iouter,1)==0, save(fname_res,'par');  end  % save results
    end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%   Test Gradient: testgrad, nested function
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function testgrad % nested function

delta=1e-8; 
TEST_HESS=0;
%rand('state',0);
u0=u;
x0=x;

%u0= 1e-0*(rand(mconstr,1)+0.5);

%x0=rand(nx,1);

if TEST_HESS
  [dphi,f0,g0,H0]=fgh(x0,penpar,u0,par);Hnum=zeros(size(H0));                
else
  [dphi,f0,g0]=fgh(x0,penpar,u0,par);
end

grdnum=zeros(size(g0));

for i=1:nx,
  x=x0;
  x(i)=x(i)+delta;
  [dphi,f,g]=fgh(x,penpar,u0,par);
  grdnum(i)=(f-f0)/delta;
  if TEST_HESS, Hnum(:,i)=(g-g0)/delta; end
end
x=x0;

figure; npl=3;
subplot(npl,1,1); plot(g0);grid; title('Analitical Gradient');
subplot(npl,1,2); plot(grdnum);grid; title('Numerical Gradient');
subplot(npl,1,3); plot(g0-grdnum);grid;title( 'Analitical - Numerical');
%subplot(npl,1,4); plot(u0);grid;

if TEST_HESS
 figure; npl=3;
 subplot(1,npl,1);imagesc(abs(H0));grid;colorbar; title('Analitical Hessian');
 subplot(1,npl,2);imagesc(abs(Hnum));grid;colorbar; title('Numerical Hessian');
 subplot(1,npl,3);imagesc(abs(H0-Hnum));grid;colorbar; title('Analitical- Numerical');

end
% H0
% Hnum
% H0byHnum=H0./Hnum
return



figure;plot([H0(:,13) Hnum(:,13)]);
%g0,grdnum-g0
end %testgrad



end  %% pbmfun



