function AA = mult_jacob_A_tensor(x,par)

[mx,nx,L]=size(par.Y);
n=mx*nx;
X=reshape(full(x),mx,nx,L);


k=0;
for i=1:mx
	for j=1:nx
      k=k+1;
		AA{k}= -[X(i,j,1), X(i,j,2); X(i,j,2),X(i,j,3)];
	end
end
