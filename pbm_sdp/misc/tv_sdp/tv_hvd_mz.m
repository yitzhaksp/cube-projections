function [f,grad,Hz]=tv_hvd_mz(X,eps,Z,par)
%
%Smoothed Total Variation horisontal-vertical-diagonal approximation
%
%
% Michael Zibulevsky,  20.12.2011

w=1; %1/sqrt(2); % diagonal internal weight  in expression u*phi(w*Dd1x) 
u=1/sqrt(2);       % diagonal external weight  

D1X= D1(X); % horisintal differences
D2X= D2(X); % vertical differences

%D1Xsr=circshift(D1X,[0,1]); % shifted to the right
%D2Xsr=circshift(D2X,[0,1]); % shifted to the right

Dd1X=w*(circshift(D1X,[0,1]) + D2X);  % weighted diagonal differences
Dd2X=w*(circshift(D2X,[0,1]) + D1X);

phi1  =sqrt(D1X.^2+eps);
phi2  =sqrt(D2X.^2+eps);
phid1 =sqrt(Dd1X.^2+eps);
phid2 =sqrt(Dd2X.^2+eps);

f=sum(phi1(:)) + sum(phi2(:)) + u*(sum(phid1(:)) + sum(phid2(:)))...                       
	- (2+2*u)*length(phi1(:))*sqrt(eps);


if nargout>1,
	tmp1=1./phi1;
	tmp2=1./phi2;
	tmpd1=1./phid1;
	tmpd2=1./phid2;
	
	dphi1=D1X.*tmp1;
	dphi2=D2X.*tmp2;
	dphid1=Dd1X.*tmpd1;
	dphid2=Dd2X.*tmpd2;
	
	grad = D1adj(dphi1)+D2adj(dphi2) + ...
		(u*w)*(D1adj(circshift(dphid1,[0,-1])) + D2adj(dphid1)+...
		      D2adj(circshift(dphid2,[0,-1])) + D1adj(dphid2));
end

% if nargout > 2,           % Compute Hessian-vector product Hz  !!! NOT IMPLEMENTED YET !!!
% 	tmp_sq=tmp.^2;
% 	d2phi11=tmp - D1X.*dphi1.*tmp_sq;
% 	d2phi22=tmp - D2X.*dphi2.*tmp_sq;
% 	d2phi12=       - D1X.*dphi2.*tmp_sq;
% 
% 	K=size(Z,3); % For the case of multiple images Z
% 	for k=1:K
% 		D1z=D1(Z(:,:,k));
% 		D2z=D2(Z(:,:,k));
% 		Hz(:,:,k)=D1adj(d2phi11.*D1z + d2phi12.*D2z) + D2adj(d2phi22.*D2z + d2phi12.*D1z);
% 	end
% 
% end

end

function Y=D1(X)
[m,n]=size(X);
Y=[diff(X,1,1);zeros(1,n)];
end

function Y=D2(X)
[m,n]=size(X);
Y=[diff(X,1,2) zeros(m,1)];
end


function  Z=D1adj(Y)
Z=[-Y(1,:); -diff(Y,1,1)];  Z(end,:)=Z(end,:)+Y(end,:);
end

function  Z=D2adj(Y)
Z=[-Y(:,1)  -diff(Y,1,2)];   Z(:,end)=Z(:,end)+ Y(:,end);
end

%function fdummy
%%%%%%%%%%%%%%%%%%%%%%%%%% Test %%%%%%%%%%





