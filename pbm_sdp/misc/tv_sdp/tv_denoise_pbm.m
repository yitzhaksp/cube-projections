%function tv_denoise_pbm() 
%
%  min ||c||_1
%  s.t. mean(psi_i(A'Ac - y)) <= b_i, i=1,2,.., k  
%  phi_i convex even non=negative functions for computing moments of residual' distribution.
%  b_i - corresponding moment of gaussian distribution ( + 4 sigma of its)
%

%  min 1/2 x'Qx + c'x
%  s.t. Ax-b <= 0
%
%  matrix Q - symmetric positive semidefinite

% MZ  16.08.2011

addpath(genpath('..'))
%par=[];

GENERATE_DATA=0;

mx=128;nx=mx;  % image size

par.name= 'tv_denoise_pbm';
par.fungrad = @tv_denoise_pbm_fgh; % User function for computing function, grad & Hess of the penalty aggregate

par.flag_standard_tv=0;   % 0-horizontal-vertical-diagonal penalty; 1- standard tv
par.eps_tv = 1e-4;
par.mu_tv=2e-4 * (1+2*par.flag_standard_tv);
%par.flagXnew=1;

par.sigma_noise=1e-1;


if GENERATE_DATA
	tmp=double(imread('lena.png'));tmp=tmp/max(tmp(:));
	par.X00=imresize(tmp,[mx nx]);
	par.Y=par.X00+par.sigma_noise*randn(size(par.X00));
	%figure;subplot(121);imagesc(par.X00);colorbar;subplot(122);imagesc(par.Y);colorbar
end

%return




%=========================================================================
%         SET  PARAMETERS  of  OPTIMIZATION ALGORITHM
%=========================================================================

options=pbm_optionset(); % Set standard parameters of  PBM algorithm

options.NouterTol = 1;   % max number of outer PBM iterations
options.N_ucTol   = 300;  % max number of iter. for_ unconstr.opt.
options.ucDXtol   = 1e-7; % accuracy of  unconstr.opt. in x
options.ucDFtol   = 1e-7; % accuracy unconstr.opt. in f
options.normGrdTol= 1e-8; % for_ Newton method

options.p_tol=1e-4;         % minimal penalty parameter
options.DFtol=1e-20;        % stopping crireria for PBM
options.INFEAStol=1e-20;    % change in the objectiive and infeasibility


%%%Unconstrained optimization tool to use :
%options.ucTool='newton_';  % For medium-size problems, when Hessian is available
options.ucTool='minfunc';  % For medium and large problems: L-BFGS minFunc of Mark Schmidt, http://www.cs.ubc.ca/~schmidtm
%options.ucTool='fminunc';   % BFGS from matlab optimization toolbox
%options.ucTool='myfminu';   % old MATLAB toolbox unconstrained minimization (with Mzib modification)
%options.ucTool='sesop__';

options.NLP=1;    % 1 - PBM for NLP (nonlilnear programming)
options.SDP=0;    % 1 - PBM for SDP (nonlilnear semidefinite programming)
options.SUMMAX=0; % 1 - smoothing method of multipliers for summax problem

options.testgrad=0;           % 1 - test gradient, 0 - no
options.test_hess=0;           % 1 - test hessian, 0 - no
options.update_multiplires=1; % 1 - updeta multipliers, 0 - use just penalty function method

par.optpar=options;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%      Solve the optimization problem with PBM method
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

mconstr=1;  % number of constraints
u0=ones(mconstr,1);           % Initiate Lagrange multipliers 
%x0=randn(size(par.X00(:)));
x0=zeros(size(par.X00(:)));
[x,u,par]=pbmsdp(x0,u0,par); % Call solver
	
X=reshape(x, size(par.X00));
figure;subplot(221);imagesc(par.X00);colorbar;title('Original');colormap(gray);
subplot(222);imagesc(par.Y);colorbar;title('Noisy');
subplot(223);imagesc(X);colorbar;title('Denoised with TV');

