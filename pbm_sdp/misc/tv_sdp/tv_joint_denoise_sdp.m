%function tv_joint_denoise_sdp() 
%

% MZ  16.08.2011

addpath(genpath('..'))
%par=[];

GENERATE_DATA=1;

mx=32;nx=mx; Lx=3;% image size
mA=2; % sdp matrix size
 
par.name= 'tv_joint_denoise_sdp';
par.fungrad = @fgh1_sdp; % User function for computing function, grad & Hess of aggregate
par.fg_obj= @tv_joint_denoise_fgh; % User function for computing function, grad & Hess of the penalty aggregate

par.eval_A =@mult_jacob_A_tensor;
par.mult_jacob_A= @mult_jacob_A_tensor;
par.mult_adj_jacob_A= @mult_adj_jacob_A_tensor;

par.flag_standard_tv=1;   % 0-horizontal-vertical-diagonal penalty; 1- standard tv
par.eps_tv = 1e-4;
par.mu_tv=3e-3 * (1+2*par.flag_standard_tv);
%par.mu_tv=0;
%par.flagXnew=1;

par.sigma_noise=0.1; %1e-1;


if GENERATE_DATA
	par.X00=[];
	tmp=double(imread('lena.png'));
	tmp=phantom(mx);
	tmp=tmp/max(tmp(:));
	par.X00(:,:,1)=imresize(tmp,[mx nx]);
	par.X00(:,:,2)=sin(2*par.X00(:,:,1));
	par.X00(:,:,3)=cos(2*par.X00(:,:,1));
	par.Y=par.X00+par.sigma_noise*randn(size(par.X00));
% 	figure;
% 	for i=1:Lx,
% 		subplot(1,Lx,i);imagesc(par.X00(:,:,i));colorbar;
% 	end
	%figure;subplot(121);imagesc(par.X00);colorbar;subplot(122);imagesc(par.Y);colorbar
end

[mx,nx,Lx]=size(par.Y);

%return

par.Z=speye(numel(par.Y)); % computing HZ in fgh_sdp, we get the hessian 



%=========================================================================
%         SET  PARAMETERS  of  OPTIMIZATION ALGORITHM
%=========================================================================

options=pbm_optionset(); % Set standard parameters of  PBM algorithm

options.NouterTol = 20;   % max number of outer PBM iterations
options.N_ucTol   = 300;  % max number of iter. for_ unconstr.opt.
options.ucDXtol   = 1e-7; % accuracy of  unconstr.opt. in x
options.ucDFtol   = 1e-7; % accuracy unconstr.opt. in f
options.normGrdTol= 1e-8; % for_ Newton method

options.mu_up     = 3; %3; % relative upper bounds on multipliers update;
options.mu_down   =  1/3;  % relative lower  bounds on multipliers update;
options.u_tol     = 1e-5; % absolute lower bound on multipliers;  
options.penpar0   = 0.1;  %0.7;  % initial penalty parameter
options.pFactor   = 1/3;  % factor of change in penalty param.
options.p_tol     = 1e-6; %2e-4; % lower bound on penalty parameter;  

options.DFtol=1e-20;        % stopping crireria for PBM
options.INFEAStol=1e-20;    % change in the objectiive and infeasibility

options.Ngad_uncTol = 3;    % num of grad steps in frozen Hessian

%%%Unconstrained optimization tool to use :
%options.ucTool='newton_';  % For medium-size problems, when Hessian is available
options.ucTool='minfunc';  % For medium and large problems: L-BFGS minFunc of Mark Schmidt, http://www.cs.ubc.ca/~schmidtm
%options.ucTool='fminunc';   % BFGS from matlab optimization toolbox
%options.ucTool='myfminu';   % old MATLAB toolbox unconstrained minimization (with Mzib modification)
%options.ucTool='sesop__';

options.NLP=0;    % 1 - PBM for NLP (nonlilnear programming)
options.SDP=1;    % 1 - PBM for SDP (nonlilnear semidefinite programming)
options.SUMMAX=0; % 1 - smoothing method of multipliers for summax problem

options.testgrad=0;           % 1 - test gradient, 0 - no
options.test_hess=0;           % 1 - test hessian, 0 - no
options.update_multiplires=1; % 1 - updeta multipliers, 0 - use just penalty function method

par.optpar=options;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%      Solve the optimization problem with PBM method
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

mconstr=mx*nx;  % number of constraints

for i=1:mconstr,
   u0.V{i}=1e0*eye(mA);   % Initiate Lagrange multiplier matrix
end
x0=zeros(size(par.X00(:)));
%x0=randn(size(par.X00(:)));
[x,u,par]=pbmsdp(x0,u0,par); % Call solver
	
X=reshape(x, size(par.X00));
figure;
for i=1:Lx,
	subplot(3,Lx,i);imagesc(par.X00(:,:,i));colorbar;title('Original');colormap(gray);
	subplot(3,Lx,i+Lx);imagesc(par.Y(:,:,i));colorbar;title('Noisy');colormap(gray);
	subplot(3,Lx,i+2*Lx);imagesc(X(:,:,i));colorbar;title('Denoised with JTV');colormap(gray);
end

AA=par.eval_A(x,par);
mineig=[];
for i=1:numel(AA)
   mineig(i)=min(-eig(AA{i}));
   maxeig(i)=max(-eig(AA{i}));
end
figure;subplot(211);plot(mineig);grid; title('Min eigenvalue of SDP constraints')
subplot(212);plot(maxeig);grid; title('Max eigenvalue of SDP constraints')

% figure;subplot(221);imagesc(par.X00);colorbar;title('Original');colormap(gray);
% subplot(222);imagesc(par.Y);colorbar;title('Noisy');
% subplot(223);imagesc(X);colorbar;title('Denoised with TV');

