function x = mult_adj_jacob_A_tensor(AA,par)

[mx,nx,L]=size(par.Y);
k=0;
for i=1:mx
	for j=1:nx
      k=k+1;
		X(i,j,1)= AA{k}(1,1);
		X(i,j,2)= AA{k}(1,2)+AA{k}(2,1);
		X(i,j,3)= AA{k}(2,2);
	end
end
x=-X(:);