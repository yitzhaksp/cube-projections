function [f,g,H]=tv_joint_denoise_fgh(x,par)
% function, grad & Hess of penalty aggregate


fconstr=	0;
dphi=1;

[mx,nx,L]=size(par.Y);
n=mx*nx;
X=reshape(x,mx,nx,L);
scale_factor_tv= par.mu_tv/min(nx,mx);
scale_factor_r=1/n;
%scale_factor_r=1e6;


if 1 %par.flag_standard_tv
	[f_tv,g_tv]=tv_joint(X,par.eps_tv);                % usual total variation
else
	%[f_tv,g_tv]=tv_hvd_mz(X,par.eps_tv);  %,Z,par);  %Total Variation horisontal-vertical-diagonal approximation
end

%R=X-par.Y;r=R(:);
r=x-par.Y(:);

f= 0.5*scale_factor_r*(r'*r)+ scale_factor_tv*f_tv;


g=scale_factor_r*r + scale_factor_tv*g_tv(:);

if nargout==3,
   H=scale_factor_r*eye(numel(g)); % Approximation: Hessian of the quadratic term only (without TV)
end








%%%%%%%%%%%% Garbage %%%%%%%%%

%[phi,dphi,d2phi]= penfun(fconstr,penpar,u); % penalty fun. and its derivatives


%F_aggr=fobj+sum(phi);

% if nargout>4,  %%%%%%%%%%%%% Compute gradient %%%%%%%%%%%%%
% 
% 
% 	
% end
% if nargout>5,  %%%%%%%%%%%%% Compute Hessian %%%%%%%%%%%%%
% 
% error('Hessian not implemented yet')
% 
% end
