function Df = ld_dia(mod,f)

if mod=='fwd',
D1 = circshift(f,[-1,-1]) - f; % f_(i,j) - f_(i,j-1)
Df = D1(1:end-1, 1:end-1);
end
if mod=='trn',
    [n1,n2] = size(f);
 Dm1 = f-circshift(f,[-1,-1]); % f_(i,j) - f_(i,j-1)
Dm1 = Dm1(1:end-1, 1:end-1);
   Df = zeros(n1+1,n2+1);
    Df(2:end-1, 2:end-1) = Dm1;
    Df(1,1:end-1) = -f(1,:);
    Df(1:end-1,1) = -f(:,1);
    Df(end,2:end) = f(end,:);
    Df(2:end,end) = f(:,end);
end