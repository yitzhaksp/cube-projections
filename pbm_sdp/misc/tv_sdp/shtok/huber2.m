function [y,g] = huber(x,delta)
% small delta = L1 norm, % large delta = L2 norm
% computes the value and the derivative of the Huber penalty function
y = zeros(size(x));
g = zeros(size(x));

ind1 = find(abs(x)<delta);
ind2 = find(abs(x)>=delta);
    y(ind1)= 1/2*x(ind1).^2;
    g(ind1) = x(ind1);
    y(ind2) = delta*abs(x(ind2))  - delta^2/2;
    g(ind2) = sign(x(ind2))*delta;
y = y/delta;
g = g/delta;
return;

% xx = -5:0.001:5;
% figure; plot(xx,huber2(xx, 8e-1));
% hold on; plot(xx,huber2(xx, 4e-1),'r');