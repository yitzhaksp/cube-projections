function Df = ld_star(mod,f)
% assume square image
if strcmp(mod,'fwd'),
    ft = f';
    N = ld_hb('fwd',ft);
    S =ld_hb('fwd',ft(end:-1:1,:));
    W = ld_hb('fwd',f);
    E = ld_hb('fwd',f(:,end:-1:1));
    
    NW = ld_dia('fwd',f);
    NE = ld_dia('fwd',f(:,end:-1:1));
    SE = ld_dia('fwd',f(end:-1:1,end:-1:1));
    SW = ld_dia('fwd',f(end:-1:1,:));
    Df = [N;S;W;E; NE;NW;SE;SW];
end

if strcmp(mod,'trn'),
    n = size(f,2)+1;
    n1 = n-1;
    N  = f(1:n,:);
    S  = f(1*n+1:2*n,:);
    W  = f(2*n+1:3*n,:);
    E  = f(3*n+1:4*n,:);
    
    NE = f(4*n+1:4*n+n1,:);
    NW = f(4*n+1*n1+1:4*n+2*n1,:);
    SE = f(4*n+2*n1+1:4*n+3*n1,:);
    SW = f(4*n+3*n1+1:4*n+4*n1,:);
    
    
    %------------------------------------
    hbN = ld_hb('trn',N);
    hbS = ld_hb('trn',S);
    hbW = ld_hb('trn',W);
    hbE = ld_hb('trn',E);

    Nt = hbN';
    St = hbS(end:-1:1,:);    St = St';
    Wt = hbW;    
    Et = hbE(:,end:-1:1);
    %------------------------------------
    diaNW = ld_dia('trn',NW);
    diaNE = ld_dia('trn',NE);
    diaSE = ld_dia('trn',SE);
    diaSW = ld_dia('trn',SW);    
    
    NWt = diaNW;
    NEt = diaNE(:,end:-1:1);
    SEt = diaSE(end:-1:1,end:-1:1);
    SWt = diaSW(end:-1:1,:);
    
    Df = Nt + St + Wt + Et + ...
         NWt + NEt + SEt + SWt;
end
% got ld_hb (<--), ld_dia (\ up).
% Build the star with components 
% NW  N  NE
% W       E
% SW  S  SE 