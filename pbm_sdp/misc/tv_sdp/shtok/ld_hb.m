function Df = ld1(mod,f)

if mod=='fwd',
D1 = circshift(f,[0,-1]) - f; % f_(i,j) - f_(i,j-1)
Df = D1(:, 1:end-1);
end
if mod=='trn',
Dm1 = f - circshift(f,[0,-1]);
Dm1= Dm1(:, 1:end-1);
        Df = [-f(:,1), Dm1, f(:,end)];
end