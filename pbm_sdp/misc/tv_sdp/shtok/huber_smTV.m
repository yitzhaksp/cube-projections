function [R, grad_R] = huber_smTV(x)
n = size(x,1);
Dx = ld_star('fwd',x); % compute eight times larger matrix of directed diferences
[rhoDx, rho_tagDx] = huber2(Dx, eps); % compute elementwise Huber function and its derivative
R = sum(sum(rhoDx)); % total sum
grad_R = ld_star('trn',rho_tagDx)/n; % transpose of the differential operator ld_star, produces the gradient of R
return
