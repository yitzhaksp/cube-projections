A = imread('bowl_of_white_rice_066.jpg');
f0 = rgb2ycbcr(double(A)); 
f0 = f0(:,:,1);
n = min(size(f0));
f0 = f0(1:n, 1:n); %imd(f0);
[R, grad_R] = huber_smTV(f0);
%imd(grad_R)
figure;subplot(121);imagesc(f0);colorbar
subplot(122);imagesc(grad_R);colorbar