function [dphi,fobj,fconstr,F_aggr,grd_aggr,Hess_aggr]=tv_denoise_pbm_fgh(x,u,penpar,par)
% function, grad & Hess of penalty aggregate


fconstr=	0;
dphi=1;

[mx,nx]=size(par.Y);
n=mx*nx;
X=reshape(x,mx,nx);
scale_factor_tv= par.mu_tv/min(nx,mx);
scale_factor_r=1/n;


if par.flag_standard_tv
	[f_tv,g_tv]=tvmz(X,par.eps_tv);                % usual total variation
else
	[f_tv,g_tv]=tv_hvd_mz(X,par.eps_tv);  %,Z,par);  %Total Variation horisontal-vertical-diagonal approximation
end

R=X-par.Y;
r=R(:);

F_aggr= 0.5*scale_factor_r*r'*r+ scale_factor_tv*f_tv;

fobj=F_aggr;
grd_aggr=scale_factor_r*r + scale_factor_tv*g_tv(:);

%[phi,dphi,d2phi]= penfun(fconstr,penpar,u); % penalty fun. and its derivatives



%F_aggr=fobj+sum(phi);

% if nargout>4,  %%%%%%%%%%%%% Compute gradient %%%%%%%%%%%%%
% 
% 
% 	
% end
% if nargout>5,  %%%%%%%%%%%%% Compute Hessian %%%%%%%%%%%%%
% 
% error('Hessian not implemented yet')
% 
% end
