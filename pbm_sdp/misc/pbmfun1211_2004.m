function [x,u,probl]=pbmfun(x0,u0,probl),
%
% PBMFUN - penalty/barrier multiplier algorithm for nonlinear optimization
%       with functional constraints:
%
%          min fobj(x) 
%   subject to fconstr_i(x)<=0, i=1,..,nconstr
%
%
%[x,u,probl]=pbmfun(x0,u0,probl)
%
%INPUT:
%
%x0    - initial vector of variables
%u0    - initial vector of multipliers
%probl - structure with problem data and optimization parameters (see init_qp.m)
%
%OUTPUT:
%x     - resulting vector of variables
%u0    - resulting vector of multipliers
%probl - structure with problem data, optimization parameters and results

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                            M.Zibulevsky
%                                            Version: 08.04.2002
%                                                         24.08.2004
%
%   PARAMETERS:
%   ============
global N_FUN_EVAL N_GRAD_EVAL N_HESS_MULT N_HESS_EVAL; %counters of function  & gradient calls
N_FUN_EVAL=0;N_GRAD_EVAL=0;N_HESS_MULT=0;N_HESS_EVAL=0;

CONTINUE_OLD=0;   % 1 - continue old run (load probl. structure from file)
TEST_GRAD =0;     % 1 - test gradient, 0 - no

%init_probl='init_tv';   % Name of user function to init problem data
%init_probl='init_adaptdeconv';   % 
%init_probl='init_qp';   % 
%init_probl= 'init_dualspars'; 
%init_probl= 'init_dualnew';   

SUMMAX =0;         % 1 - smoothing method of multipliers for summax problem
mu_up=3;      %relative bounds on multipliers update;
mu_down=0.3; %
u_tol=1e-5;   % absolute lower bound on multipliers;  

penpar0=0.7;     % initial penalty parameter
pFactor=0.5;
p_tol=2e-5;   % absolute lower bound on penalty parameter;  

NouterTol=10000; % max number of outer iterations

N_ucTol=300;      % max number of iterations for unconstrained opt.
ucDXtol=1e-6;     % accuracy in x
ucDFtol=1e-6;     % accuracy in f
ucTool='fminu__'; % 'fminu__' (MATLAB tool) or 'fmin_uc' (TOMLAB tool)

ucMethod=0;       % options(5) for fmin_uc(), that uses TOMLAB:
    %0 = Newton, 1 = Safeguarded BFGS  2 = Safeguarded Inverse BFGS
    %3 = Safeguarded Inverse DFP       4 = Safeguarded DFP
    %5 = Fletcher-Reeves CG            6 = Polak-Ribiere CG
    %7 = Fletcher conjugate descent CG-method

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%[probl,x0,u0]=feval(init_probl);

fname_res=[probl.name '.res'];
funprintname= [probl.name '_print'];

if ~isempty(probl.optpar.summax),    SUMMAX   = probl.optpar.summax;end
if ~isempty(probl.optpar.mu_up),     mu_up    = probl.optpar.mu_up;end
if ~isempty(probl.optpar.mu_down),   mu_down  = probl.optpar.mu_down;end
if ~isempty(probl.optpar.u_tol),     u_tol    = probl.optpar.u_tol;end
if ~isempty(probl.optpar.penpar0),   penpar0  = probl.optpar.penpar0;end
if ~isempty(probl.optpar.pFactor),   pFactor  = probl.optpar.pFactor;end
if ~isempty(probl.optpar.p_tol),     p_tol    = probl.optpar.p_tol;end
if ~isempty(probl.optpar.NouterTol), NouterTol= probl.optpar.NouterTol;end
if ~isempty(probl.optpar.N_ucTol),   N_ucTol  = probl.optpar.N_ucTol;end
if ~isempty(probl.optpar.ucDXtol),   ucDXtol  = probl.optpar.ucDXtol;end
if ~isempty(probl.optpar.ucDFtol),   ucDFtol  = probl.optpar.ucDFtol;end
if ~isempty(probl.optpar.ucTool),    ucTool   = probl.optpar.ucTool;end
if ~isempty(probl.optpar.ucMethod),  ucMethod = probl.optpar.ucMethod;end

DFtol    = probl.optpar.DFtol;        % stopping crireria for PBM
INFEAStol= probl.optpar.INFEAStol;    % change in the objectiive and infeasibility


%if ~isempty(probl.optpar.), = probl.optpar.;end




if(~CONTINUE_OLD),
  x=x0(:);
  u=u0(:);
  penpar=penpar0;
  tcpu0=cputime;
  iouter0=1;
  igad_unc=1e10;

  dfobjs=[];infeas=[]; gaps=[];
  
  N_FUN_EVALs=0;
  N_GRAD_EVALs=0;
  N_HESS_EVALs=0;
  cputimes=0;
else,
  eval(['load ' fname_res ' -MAT']);
  x=probl.res.x;
  u=probl.res.u;
  penpar=probl.res.penpar;
  tcpu0=cputime-probl.res.cpu_time;
  N_FUN_EVALs=probl.res.n_fun_evals
  N_GRAD_EVALs=probl.res.n_grad_evals;
  N_HESS_EVALs=probl.res.n_Hevals;
  cputimes=probl.res.cputimes;
  dfobjs=probl.res.dfobjs;
  infeas=probl.res.infeas;
  gaps=probl.res.gaps;
  %N_HESS_MULTs=probl.res.n_Hmults;
  iouter0=probl.res.iouter;
end


nx=length(x);
mconstr=length(u);

if TEST_GRAD, testgrad;return; end


if  1 
  [dphi,fobj_old,fconstr,f_aggr]=feval(probl.fungrad, x,u,penpar,probl);
else
  [fobj_old,fconstr] = fc_problem(x,probl);
end

if SUMMAX, fobj_old=fobj_old+norm(fconstr,1); end

if(~CONTINUE_OLD), 
	dfobjs=fobj_old;
    infeas=max(fconstr);
	gaps= fobj_old-f_aggr;
end


fprintf('fobj=%g f_aggr=%g\n',fobj_old,f_aggr);



for iouter=iouter0:NouterTol, % ======OUTER ITERATION =======

  %-----------------------------------------------------------
  %      Unconstrained minimization of the penalty aggregate
  %-----------------------------------------------------------

  %[x,dphi] = minimize_aggr(x,penpar,u,probl)
  options=foptions;
  options(1)=0;           % 1-display tabular results
  options(2)=ucDXtol;        % accuracy in x
  options(3)=ucDFtol;        % accuracy in f
  %options(4)=5;           % display frequency

  options(7)=1;          % 1-cubic linesearch
  %options(9)=1;         % 1-check user-supplied gradients
  options(14)=N_ucTol;   % Maximum number of function_ evaluations.


  if ucTool=='fminunc',        disp('NEW matlab optim.toolbox');
    new_options = optimset('GradObj','on','TolFun',ucDFtol,'TolX',ucDXtol,...
			   'MaxFunEvals',N_ucTol,'LargeScale','on','Display','testing' );

    %[x,fval,exitflag,output]= fminunc('fg_aggregate',x,new_options,penpar,u,probl);
    [x,fval,exitflag,output]= fminunc_zib('fg_aggregate',x,new_options,penpar,u,probl);

  elseif ucTool=='fminu__',     % old MATLAB toolbox unconstrained minimization

    options(5)=0;           % Algorithm strategy - BFGS
    x=fminu('fun_aggregate',x,options,'grad_aggregate',penpar,u,probl);


  elseif ucTool=='fminuc_',                      % TOMLAB unconstrained minimization

    options(5)=ucMethod;    % Algorithm strategy
    x=fmin_uc('funtom',x,options,'gradtom',penpar,u,probl);
    
  elseif ucTool=='mybfgs_',     % My BFGS (M.Zibulevsky)

    if iouter==iouter0, B_aggr=eye(nx);end
    %probl.optpar.B=B_aggr; % Approximation of inverse Hessian for next call BFGS
    %[x,f_aggr,g_aggr,B_aggr]=mybfgs('fgh_aggregate',x,probl.optpar,penpar,u,probl);
	[x,f_aggr,g_aggr,B_aggr]=BFGS_ulbmz('fgh_aggregate',x,B_aggr,probl.optpar, penpar,u,probl);


  elseif ucTool=='newton_'

    %mynewton;
    newt_frozhess;

  else 
    error('Unknown ucTool');
  end

  if  1 % fungrad already includes penalty 
    [dphi,fobj,fconstr,f_aggr,g_aggr]=feval(probl.fungrad,x,u,penpar,probl);
  else
    [fobj,fconstr] = fc_problem(x,probl);
    [dphi,f_aggr,g_aggr]=fgh(x,penpar,u,probl);
  end

 
 
  if SUMMAX,
   fobj=fobj+norm(fconstr,1);
   gap=fobj-f_aggr;
   gaps=[gaps;gap];
   dfobj=fobj-fobj_old;
   fprintf('\n******Out_iter #%d  penpar=%.2e normgrd=%.2e gap=%.4e dfobj=%.3e ****\n', ...
	  iouter,penpar,norm(g_aggr),gap,dfobj);
   fprintf('fobj=%.12g f_aggr=%g Nfun=%g Ngrad=%g NH=%g NHmult=%g Cpu_time=%g\n',...
   fobj,f_aggr,N_FUN_EVAL,N_GRAD_EVAL, N_HESS_EVAL,N_HESS_MULT, cputime-tcpu0);
  
  else
   dfobj=fobj-fobj_old;
   fprintf('\n******Out_iter #%d  penpar=%.2e normgrd=%.2e infeas=%.4e dfobj=%.3e ****\n', ...
	  iouter,penpar,norm(g_aggr),max(fconstr),dfobj);
   fprintf('fobj=%g f_aggr=%g Nfun=%g Ngrad=%g NH=%g NHmult=%g Cpu_time=%g\n',...
   fobj,f_aggr,N_FUN_EVAL,N_GRAD_EVAL, N_HESS_EVAL,N_HESS_MULT, cputime-tcpu0);
  
  end

   dfobjs= [dfobjs;dfobj];
   infeas=[infeas;max(fconstr)];
   N_FUN_EVALs=[N_FUN_EVALs;N_FUN_EVAL];
   N_GRAD_EVALs=[N_GRAD_EVALs;N_GRAD_EVAL];
   N_HESS_EVALs=[N_HESS_EVALs;N_HESS_EVAL];
   cputimes=[cputimes;cputime-tcpu0];

  fobj_old=fobj;
  %fconstr
  %pbm_print; % print intermidiate results
 %% eval(probl.print);
  %plot(u);grid;drawnow;
  
  probl.res.x=x;
  probl.res.u=u;
  probl.res.penpar=penpar;
  probl.res.infeas=max(fconstr);
  probl.res.normgrd=norm(g_aggr);
  probl.res.dfobj=fobj-fobj_old;
  probl.res.f_aggr=f_aggr;
  probl.res.fobj=fobj;
  probl.res.iouter=iouter;
  probl.res.cpu_time=cputime-tcpu0;
  probl.res.n_fun_evals=N_FUN_EVALs;
  probl.res.n_grad_evals=N_GRAD_EVALs;
  probl.res.n_Hevals=N_HESS_EVALs;
  %probl.res.n_Hmults=N_HESS_MULTs;
  probl.res.cputimes=cputimes;
  probl.res.dfobjs=dfobjs;
  probl.res.infeas=infeas;
  probl.res.gaps=gaps;


   if rem(iouter,1)==0, save(fname_res,'probl');  end  % save results

  %-----------------------------------------------------------
  %           Update multipliers and penalty parameter
  %-----------------------------------------------------------

  if SUMMAX,
     if(mu_up ~=1)
       alpha= -1;
       beta=1;
       u1=min(mu_up*(u-alpha), dphi-alpha) +alpha;
       u2=max(mu_down*(u-alpha),u1-alpha) +alpha;
       u3 = beta - min(mu_up*(beta-u), beta-u2);  %beta - u3 = min(mu_up*(beta-u), beta-u2);
       u4 = beta - max(mu_down*(beta-u), beta - u3);
       u5=min(u4,beta-u_tol);  
       u=max(u5,alpha+u_tol); 
     end
  else  
     u1=min(dphi,mu_up*u); u2=max(u1,mu_down*u); u=max(u2,u_tol);
  end

  penpar=max(penpar*pFactor,p_tol);  
  
  
  %-----------------------------------------------------------
  %           Show results
  %-----------------------------------------------------------
 
  if rem(iouter,1)==0, 
      %codex_print(x,u,penpar,probl);
      feval(funprintname,x,u,penpar,probl); % print/plot results
      mplots=1;
      nplots=1;
      figure(100);
      if SUMMAX
          subplot(mplots,nplots,1); semilogy(cputimes, abs((fobjs-(fobjs(end)))/max(fobjs)),'o',...
              cputimes, abs(gaps/max(fobjs)),'.');grid; title(' ')
          %subplot(mplots,nplots,1); semilogy(cputimes, gaps,'o');grid
          grid; title('abs((fobjs-(fobjs(end)))/max(abs(fobjs)))')
      else
		
        subplot(mplots,nplots,1); semilogy(cputimes, abs(dfobjs)+1e-20,'o', ...
               cputimes, abs(infeas)+1e-20,'.');grid;
        xlabel('CPU time, s')
        title('PBM:  Circles - change in objective function per outer iteration; Dots - max. constraint violation')

        if max(fconstr) <= -INFEAStol & abs(dfobj)<DFtol, 
			disp('PBM: stopping criteria achieved');  keyboard;%return; 
		end  % stopping crireria for PBM
        
      end
       
      drawnow;
  end
end % endof outer loop

%print -depsc2 MA_uncon.eps;

% k=2*probl.mcoefs; [u(k+1)-u(k+2), u(k+3)-u(k+4)]*probl.Amix
% 
% uu=u(1:probl.mcoefs)-u(probl.mcoefs+1:2*probl.mcoefs);
% sparse(uu .* (abs(uu)>0.01)), sparse(probl.coef)
% 

return


