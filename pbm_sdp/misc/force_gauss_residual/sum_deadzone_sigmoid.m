function  [fsum,g,HZ,diagH,f]=sum_deadzone_sigmoid(x,Z,par);
% Penalty for  vector x:  mu*sum(|x|), 
%    |.| is smoothed with parameter par.eps_smooth_abs:
%                       h(u) = abs(u) + 1./(abs(u)+1) - 1;
%                       f(x) = eps*h(x/eps)
%
% Call: [fsum,g,HZ,diagH]=sum_fast_abs_smooth_new(x,Z,par)
%
% Input:
%    x - separating vector w=[w;b] in SVM
%    Z - matrix to be multiplied by the Hessian (if needed)
%
% Output: function value, gradient, Hessian*Z, and diagonal of Hessian
%
% if par.flagXnew==0, then old persistent values of inner variables are reused 

% Michael Zibulevsky, 29.08.2011    
%
% Copyright (c) 2011. All rights reserved. Free for academic use. No warranty 

% persistent flagF flagG flagH fsum_old g_old  tau abs_tau tmp tmpsq  tau1 abs_tau1 tmp1 tmpsq1 mud2f 


mu=par.weight_abs_penalty;
eps=par.eps_sigmoid;
delta=par.delta_deadzone;

flagXnew=1;

[f1,df1,d2f1]=sigmoid_mz(x-delta,eps,flagXnew);
[f2,df2,d2f2]=sigmoid_mz(x+delta,eps,flagXnew);
f0=sigmoid_mz(delta,eps,flagXnew);
c=mu/f0;
f=c*(f1-f2 + 2*f0);
fsum=sum(f(:));
g=c*(df1-df2);
HZ=[];
diagH=c*(d2f1-d2f2);

return

% %%%%%%%%%%%%%%%   Test %%%%%%%%%%%%%%%%%%%%%% 
% 

par.eps_sigmoid=0.1;
par.delta_deadzone = 0.1;
par.flagXnew=1;
dx=0.001;
x=[-10:dx:10]';
[fsum,g,HZ,diagH,f]=sum_deadzone_sigmoid(x,[],par);
gnum= [diff(f)/dx; 0];
diagHnum=[diff(g)/dx; 0];
figure;
subplot(311);plot(x,f);grid
subplot(312);plot(x,[g gnum]);title('g gnum');grid
subplot(313);plot(x,[diagH diagHnum]);title('diagH diagHnum');grid



