%function force_gauss_residual() 
%
%  min ||c||_1
%  s.t. mean(psi_i(A'Ac - y)) <= b_i, i=1,2,.., k  
%  phi_i convex even non=negative functions for computing moments of residual' distribution.
%  b_i - corresponding moment of gaussian distribution ( + 4 sigma of its)
%

%  min 1/2 x'Qx + c'x
%  s.t. Ax-b <= 0
%
%  matrix Q - symmetric positive semidefinite

% MZ  16.08.2011

addpath(genpath('..'))
%par=[];

GENERATE_DATA=1;

par.name= 'force_gaus_resid';
par.fungrad = @fgh_force_gaus_resid; % User function for computing function, grad & Hess of the penalty aggregate

nx=512; % signal size
nc=nx; % number of coefs

par.eps_smooth_abs = 0.01;
par.eps_sigmoid    = 0.1;
par.flagXnew=1;



par.sigma_noise=1e-1;

%A=dctmtx(nc);
A=eye(nc);
par.multA= @(c, par)  A*c;
par.multAt= @(y, par) (y'*A)';

if GENERATE_DATA
	par.c00=sign(full(sprandn(nc,1,0.1)));
	par.c00=abs(par.c00);
	par.y00=par.multA(par.c00, par);
	par.y=par.y00+par.sigma_noise*randn(size(par.y00));
	%figure;plot(c00);hold on; plot(y00,'r')
end

f=[];
for i=1:300
	z=par.sigma_noise*randn(nx,1);
	f(:,i)=nonlin_moments(z,par);
end
mean_f=mean(f,2);
std_f=std(f,0,2);
max_f=max(f,[],2);

%par.b=mean_f+3*std_f;
%par.b=mean_f;
par.b=max_f;


%par.b(2)=par.b(1); % negative constraint for   usual mean

% figure;
% for i=1:size(f,1),
% 	subplot(3,3,i);hist(f(i,:))
% end

%return




%=========================================================================
%         SET  PARAMETERS  of  OPTIMIZATION ALGORITHM
%=========================================================================

options=pbm_optionset(); % Set standard parameters of  PBM algorithm

options.NouterTol = 20;   % max number of outer PBM iterations
options.N_ucTol   = 300;  % max number of iter. for_ unconstr.opt.
options.ucDXtol   = 1e-7; % accuracy of  unconstr.opt. in x
options.ucDFtol   = 1e-7; % accuracy unconstr.opt. in f
options.normGrdTol= 1e-8; % for_ Newton method

options.p_tol=1e-4;         % minimal penalty parameter
options.DFtol=1e-20;        % stopping crireria for PBM
options.INFEAStol=1e-20;    % change in the objectiive and infeasibility


%%%Unconstrained optimization tool to use :
%options.ucTool='newton_';  % For medium-size problems, when Hessian is available
options.ucTool='minfunc';  % For medium and large problems: L-BFGS minFunc of Mark Schmidt, http://www.cs.ubc.ca/~schmidtm
%options.ucTool='fminunc';   % BFGS from matlab optimization toolbox
%options.ucTool='myfminu';   % old MATLAB toolbox unconstrained minimization (with Mzib modification)
%options.ucTool='sesop__';

options.NLP=1;    % 1 - PBM for NLP (nonlilnear programming)
options.SDP=0;    % 1 - PBM for SDP (nonlilnear semidefinite programming)
options.SUMMAX=0; % 1 - smoothing method of multipliers for summax problem

options.testgrad=0;           % 1 - test gradient, 0 - no
options.test_hess=0;           % 1 - test hessian, 0 - no
options.update_multiplires=1; % 1 - updeta multipliers, 0 - use just penalty function method

par.optpar=options;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%      Solve the optimization problem with PBM method
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

mconstr=length(par.b);  % number of constraints
c0=zeros(nc,1);          % Initiate variables
u0=ones(mconstr,1);           % Initiate Lagrange multipliers 

%[x,u,par]=pbm(x0,u0,par); % Call solver
[c,u,par]=pbmsdp(c0,u0,par); % Call solver
x=par.multA(c,par);
%figure;plot([par.c00 c])

[par.b nonlin_moments(c-par.y,par),nonlin_moments(par.y-par.y00,par)]

figure; subplot(221);plot(c);grid; title('c')
subplot(223);plot(c-par.y);grid; title('c-y')
subplot(222);hist(par.y00-par.y,50);grid; title('noise')
%subplot(222);plot(par.y);grid; title('y - noisy signal')
subplot(224);hist(c-par.y,50);grid; title('c-y');
fprintf('err_out/noise_in: %g',sumsqr(x-par.y00)/sumsqr(par.y-par.y00));