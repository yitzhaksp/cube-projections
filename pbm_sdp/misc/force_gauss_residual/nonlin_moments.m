function [f,g,diagH]=nonlin_moments(x,par)


x=x(:);
n=length(x);

K=20;  % max number of moments to compute (to reserve memory)

par.weight_abs_penalty=1/2/n;
%par.eps_smooth_abs=0.01;
par.flagXnew=1;



f=zeros(K,1);
g=zeros(n,K);
diagH=zeros(n,K);

k=0;

% k=k+1;
% f(k)=1/n*sum(x);
% g(:,k)=1/n*ones(n,1);
% 
% k=k+1;
% f(k)=-1/n*sum(x);
% g(:,k)=-1/n*ones(n,1);

k=k+1;
f(k)   = 1/n*sum(x.^2);
g(:,k) = 2/n*x;
diagH(:,k) = 2/n*ones(n,1);

% k=k+1;
% abs_x=abs(x);
% f(k)   = 1/n*sum(abs_x.^3);
% g(:,k) = 3/n*abs_x.*x;
% diagH(:,k) = 6/n*abs_x;
% 
% 
% k=k+1;
% f(k)   = 1/n*sum(x.^4);
% g(:,k) = 4/n*x.^3;
% diagH(:,k) = 12/n*x.^2;

% k=k+1;
% par.delta_deadzone = 0.5*par.sigma_noise;
% [f_tmp,g_tmp,dummy,diagH_tmp]=sum_deadzone_sigmoid(x,[],par);
% f(k)=f_tmp; g(:,k)=g_tmp;diagH(:,k)=diagH_tmp;
% 
% k=k+1;
% par.delta_deadzone = 1*par.sigma_noise;
% [f_tmp,g_tmp,dummy,diagH_tmp]=sum_deadzone_sigmoid(x,[],par);
% f(k)=f_tmp; g(:,k)=g_tmp;diagH(:,k)=diagH_tmp;
% 
% k=k+1;
% par.delta_deadzone = 1.7*par.sigma_noise;
% [f_tmp,g_tmp,dummy,diagH_tmp]=sum_deadzone_sigmoid(x,[],par);
% f(k)=f_tmp; g(:,k)=g_tmp;diagH(:,k)=diagH_tmp;
% 
% k=k+1;
% par.delta_deadzone = 2*par.sigma_noise;
% [f_tmp,g_tmp,dummy,diagH_tmp]=sum_deadzone_sigmoid(x,[],par);
% f(k)=f_tmp; g(:,k)=g_tmp;diagH(:,k)=diagH_tmp;
% 
% k=k+1;
% par.delta_deadzone = 2.5*par.sigma_noise;
% [f_tmp,g_tmp,dummy,diagH_tmp]=sum_deadzone_sigmoid(x,[],par);
% f(k)=f_tmp; g(:,k)=g_tmp;diagH(:,k)=diagH_tmp;
% 
% k=k+1;
% par.delta_deadzone = 3*par.sigma_noise;
% [f_tmp,g_tmp,dummy,diagH_tmp]=sum_deadzone_sigmoid(x,[],par);
% f(k)=f_tmp; g(:,k)=g_tmp;diagH(:,k)=diagH_tmp;


% k=k+1;
% par.delta_deadzone = 4*par.sigma_noise;
% [f_tmp,g_tmp,dummy,diagH_tmp]=sum_deadzone_sigmoid(x,[],par);
% f(k)=f_tmp; g(:,k)=g_tmp;diagH(:,k)=diagH_tmp;


% 
% k=k+1;
% par.delta_deadzone = 0*par.sigma_noise;
% [f_tmp,g_tmp,dummy,diagH_tmp]=sum_deadzone_lin_smooth(x,[],par);
% f(k)=f_tmp; g(:,k)=g_tmp;diagH(:,k)=diagH_tmp;
% 
% k=k+1;
% par.delta_deadzone = 1*par.sigma_noise;
% [f_tmp,g_tmp,dummy,diagH_tmp]=sum_deadzone_lin_smooth(x,[],par);
% f(k)=f_tmp; g(:,k)=g_tmp;diagH(:,k)=diagH_tmp;
% 
% k=k+1;
% par.delta_deadzone = 2*par.sigma_noise;
% [f_tmp,g_tmp,dummy,diagH_tmp]=sum_deadzone_lin_smooth(x,[],par);
% f(k)=f_tmp; g(:,k)=g_tmp;diagH(:,k)=diagH_tmp;
% 
% k=k+1;
% par.delta_deadzone = 3*par.sigma_noise;
% [f_tmp,g_tmp,dummy,diagH_tmp]=sum_deadzone_lin_smooth(x,[],par);
% f(k)=f_tmp; g(:,k)=g_tmp;diagH(:,k)=diagH_tmp;
% % 
% k=k+1;
% par.delta_deadzone = 4*par.sigma_noise;
% [f_tmp,g_tmp,dummy,diagH_tmp]=sum_deadzone_lin_smooth(x,[],par);
% f(k)=f_tmp; g(:,k)=g_tmp;diagH(:,k)=diagH_tmp;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

f=f(1:k); g=g(:,1:k);diagH =diagH(:,1:k);
