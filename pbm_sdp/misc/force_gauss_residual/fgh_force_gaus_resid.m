function [dphi,fobj,fconstr,F_aggr,grd_aggr,Hess_aggr]=fgh_force_gaus_resid(c,u,penpar,par)
% function, grad & Hess of penalty aggregate
%  min ||c||_1
%  s.t. mean(psi_i(A'Ac-y)) <= b_i, i=1,2,.., k  
%  phi_i convex even non=negative functions for computing moments of residual' distribution.
%  b_i - corresponding moment of gaussian distribution ( + 4 sigma of its)

c=c(:);
nc=length(c);
par.weight_abs_penalty=1/nc;
[fobj,grd_obj]=sum_fast_abs_smooth_new(c,[],par);

s=par.multA(c,par);  % recovered signal 
r=s-par.y;           % residual
z=par.multAt(r,par); % analysis of residual (should be gaussian with unit marginal variance)


[fmom, gmom]=nonlin_moments(z,par);

fconstr=	fmom-par.b;


[phi,dphi,d2phi]= penfun(fconstr,penpar,u); % penalty fun. and its derivatives



F_aggr=fobj+sum(phi);

if nargout>4,  %%%%%%%%%%%%% Compute gradient %%%%%%%%%%%%%


	grd_aggr=grd_obj+ par.multAt(par.multA(gmom,par),par)*dphi;

end
if nargout>5,  %%%%%%%%%%%%% Compute Hessian %%%%%%%%%%%%%

error('Hessian not implemented yet')
%   A1=mulmd(A',d2phi);  % A1=A'*diag(d2phi);
%   Hess_aggr= Q + A1*A;

end
