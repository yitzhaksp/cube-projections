% Dear friends,
% I'd like to share some thoughts regardint superresolution reconstruction of a short pulse.
% I was motivated by examples of superresolution 

FWHM=360 %samples


t=linspace(-20,20,4001)';
delta=1e-7;
h=sin(t+delta)./(t+delta);

d=50;
H=convmtx(h,d-1);
H=H(d:end-d,:);
lam=svd(H);

figure;imagesc(H);colorbar;
figure;
subplot(211);plot(h); title('impulse responce');grid
subplot(212);semilogy(lam(1:10)+1e-7);title('singular values of convolution matrix');grid

n=0;dd=0;res_improvement=0;
d=5;
for i=1:9
   H=convmtx(h,d-1);
   H=H(d:end-d,:);
   lam=svd(H);
   dd(i)=d;
   n(i)=length(find(lam>1e-3*max(lam)));
   subplot(212);semilogy(lam+1e-7);title('singular values of convolution matrix');grid

   d=d*2;
   res_improvement(i)=dd(i)/n(i)/FWHM;
end
figure;
subplot(211); plot(dd,n);
title('Num of sing values > 1e-3');

subplot(212); plot(dd,res_improvement);xlabel('Width of pulse in samples ')
title('resolution improvement comparing to FWHM=360 samples');
xlabel('Width of pulse in samples ')
