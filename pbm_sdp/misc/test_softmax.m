% test soft min 
clear

a=3; % parameter of softmin


% generate example functions:
t=[-10:0.1:10]';
y(:,1)=t.^2;
y(:,2)=(t-5).^2;


% my softmin:
z= (sum(y.^(-a),2)).^(-1/a); 

figure;plot([y z]);legend('f1', 'f2', sprintf('softmin(f1,f2); a=%d',a));
