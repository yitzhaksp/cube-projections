function optpar=optpar_summax,

%=========================================================================
%          PARAMETERS  of  PBM ALGORITHM for summax optimization
%=========================================================================

optpar.summax =1;         % 1 - smoothing method of multipliers for summax problem; 0 - constrained optimization
optpar.proxpar   = 1e-20; % quadratic regularization term 
optpar.p_tol     = 1e-5; %2e-4; % lower bound on penalty parameter;  
optpar.mu_up     = 1.4; %3;    % relative upper bounds on
                                      % multipliers update;
optpar.mu_down   = 0.7;  % relative lower  bounds on
                                      % multipliers update;
optpar.u_tol     = 1e-6; % absolute lower bound on multipliers;  
optpar.penpar0   = 0.1; %0.7;  % initial penalty parameter
optpar.pFactor   = 0.3;  % factor of change in penalty param.
optpar.NouterTol = 500;  % max number of outer iterations
optpar.N_ucTol   = 300;  % max number of iter. for_ unconstr.opt.
optpar.ucDXtol   = 1e-6; % accuracy of  unconstr.opt. in x
optpar.ucDFtol   = 1e-6; % accuracy unconstr.opt. in f
optpar.normGrdTol= 1e-5; % for_ Newton method
optpar.Ngad_uncTol=3;    % num of grad steps in frozen Hessian
optpar.NnewtTol  = 40;   % max number of Newton steps for_ one unconstr. opt

optpar.ucTool='newton_' ;%'fminunc'(new matlab), 'fminu__' (MATLAB) or 'fmin_uc'(TOMLAB) or 'mybfgs_'
%optpar.ucTool='mybfgs_';

optpar.ucMethod  = 5;    % options(5) if_ fmin_uc() is used,
                               % calls TOMLAB:
    %0 = Newton, 1 = Safeguarded BFGS   2 = Safeguarded Inverse BFGS
    %3 = Safeguarded Inverse DFP        4 = Safeguarded DFP
    %5 = Fletcher-Reeves CG             6 = Polak-Ribiere CG
    %7 = Fletcher conjugate descent CG-method

