%solve_sdp: Generate semidefinite programming problem and solve it  with PBM
%
%  min 1/2 x'Qx + c'x
%  s.t. Ax-b <= 0
%
%  matrix Q - symmetric positive semidefinite

% Michael Zibulevsky 2002 - 10.02.2009 - 28.10.2010

addpath(genpath('.'))
%addpath(genpath('C:\Users\mzibulevsky\Desktop\work-related\SESOP\SESOP_PACK\SesopTN2'))
%addpath(genpath('..\SESOP_PACK\SesopTN2'))

probl.name= 'sdp';
probl.fungrad = @fgh_sdp; % User function for computing function, grad & Hess of
% the penalty aggregate
						 
INIT_DATA=0; % 1 - init data, 0- use old one from previous run

						  
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%     Generate data for quadratic programming problem
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

						  
% Create SDP matrices: A(x)= A0 +sum x_i Ai

m=5; %matrix size

n=3; % x - size


if  INIT_DATA
   
   if 0
	  
	  load ../sdp_matFormat/theta3.mat
	  probl.A0=-mat(c);
	  probl.Ai=A;
	  probl.c=-b;
	  
   else
	  
	  A0=randn(m); A0= -A0'*A0; % random negative definite matrix
	  
	  probl.A0=A0;
	  
	  for i=1:n,
		 tmp=randn(m); tmp=-tmp*tmp';
		 probl.Ai(i,:) = tmp(:)';
	  end
	  
	  
	  probl.c= rand(n,1);
	  
	  %save probl1 probl
   end
   
else 
   load probl_3_5
end

m=size(probl.A0,1);
n=length(probl.c);

probl.eval_A = @eval_A_linear;
probl.mult_adj_jacob_A= @mult_adj_jacob_A_linear;
probl.mult_jacob_A= @mult_jacob_A_linear;


probl.fg_obj=@fg_linear;
probl.Z=eye(n); % computing HZ in fgh_sdp, we get th hessian 

%=========================================================================
%         SET  PARAMETERS  of  OPTIMIZATION ALGORITHM
%=========================================================================

probl.optpar=pbm_optionset(); % Set standard parameters of  PBM algorithm

probl.optpar.NouterTol = 10;    % max number of outer PBM iterations
probl.optpar.N_ucTol   = 50;    % max number of iter. for_ unconstr.opt.
probl.optpar.ucDXtol   = 1e-8;  % accuracy of  unconstr.opt. in x
probl.optpar.ucDFtol   = 1e-7;  % accuracy unconstr.opt. in f
probl.optpar.normGrdTol= 1e-5;  % for_ Newton method
probl.optpar.Ngad_uncTol= 10;   % num of grad steps in frozen Hessian


probl.optpar.mu_up=5 ;
probl.optpar.mu_down= 1/probl.optpar.mu_up;%0.3000;
probl.optpar.u_tol= 1e-6;

probl.optpar.penpar0= 0.1000;
probl.optpar.pFactor= 0.3;
probl.optpar.p_tol=1e-6;

probl.optpar.DFtol=1e-20;        % stopping crireria for PBM
probl.optpar.INFEAStol=1e-20;    % change in the objectiive and infeasibility

%%%Unconstrained optimization tool to use :
probl.optpar.ucTool='newton_';  % For medium-size problems, when Hessian is available
%probl.optpar.ucTool='minfunc';  % For medium and large problems: L-BFGS minFunc of Mark Schmidt, http://www.cs.ubc.ca/~schmidtm
%probl.optpar.ucTool='fminunc';   % BFGS from matlab optimization toolbox
%probl.optpar.ucTool='myfminu';   % old MATLAB toolbox unconstrained minimization (with Mzib modification)
%probl.optpar.ucTool='sesop__';

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%      Solve the optimization problem with PBM method
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

x0=zeros(n,1);  % Initiate variables
x0=randn(n,1);

tmp=rand(m);
%u0.V{1}=tmp'*tmp;   % Initiate Lagrange multiplier matrix
u0.V{1}=eye(m);   % Initiate Lagrange multiplier matrix

par.optpar.SDP=1;        % 1 - PBM for SDP (nonlilnear semidefinite programming)
par.optpar.NLP=0;        % 1 - PBM for NLP (nonlilnear  programming)

[x,u,probl]=pbmsdp(x0,u0,probl); % Call solver

A_opt=probl.eval_A(x,probl);
figure;subplot(121);plot(eig(A_opt{1}));grid; title('Eig(A)')
subplot(122);semilogy(eig(probl.res.u.V{1}));grid; title('Eig(v)')







