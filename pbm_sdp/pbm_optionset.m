function options=pbm_optionset()
% Set parameters of  PBM algorithm

% Michael Zibulevsky  10.02.2009

options.proxpar   = 1e-20; % quadratic regularization term 
options.mu_up     = 3; %3; % relative upper bounds on multipliers update;
options.mu_down   = 0.3;  % relative lower  bounds on multipliers update;
options.u_tol     = 1e-5; % absolute lower bound on multipliers;  
options.penpar0   = 0.1;  %0.7;  % initial penalty parameter
options.pFactor   = 0.3;  % factor of change in penalty param.
options.p_tol     = 1e-4; %2e-4; % lower bound on penalty parameter;  
options.NouterTol = 50;   % max number of outer iterations
options.N_ucTol   = 300;  % max number of iter. for_ unconstr.opt.
options.ucDXtol   = 1e-6; % accuracy of  unconstr.opt. in x
options.ucDFtol   = 1e-8; % accuracy unconstr.opt. in f
options.normGrdTol= 1e-5; % for_ Newton method
options.NnewtTol  = 40;   % max number of Newton steps for_ one unconstr. opt
options.Ngad_uncTol = 3;    % num of grad steps in frozen Hessian
options.NLP=0;    % 1 - PBM for NLP (nonlilnear programming)
options.SDP=0;    % 1 - PBM for SDP (nonlilnear semidefinite programming)
options.SUMMAX=0; % 1 - smoothing method of multipliers for summax problem

options.DFtol=1e-8;        % stopping crireria for PBM
options.INFEAStol=1e-8;    % change in the objective and infeasibility


%%%Unconstrained optimization tool to use :
%options.ucTool='newton_';  % For medium-size problems, when Hessian is available
options.ucTool='minfunc';  % For medium and large problems: L-BFGS minFunc of Mark Schmidt, http://www.cs.ubc.ca/~schmidtm
%options.ucTool ='LBFGS_C';   % L-BFGS in C by Naoaki Okazaki
%options.ucTool='fminunc';   % BFGS from matlab optimization toolbox
%options.ucTool='myfminu';   % old MATLAB toolbox unconstrained minimization (with Mzib modification)
%options.ucTool='sesop__';


options.testgrad=0;           % 1 - test gradient, 0 - no
options.test_hess=0;           % 1 - test hessian, 0 - no
options.update_multiplires=1; % 1 - updeta multipliers, 0 - use just penalty function method

options.user_print=0;  % 1 - call user function to print/plot intermediate results

