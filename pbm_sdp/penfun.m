function  [phi,dphi,d2phi]= penfun(fconstr,penpar,multipliers)


%function  [phi,dphi]= penfun(fconstr,penpar,multipliers),
%Quadratic-logarithmic penalty function
%quadratic   - fot t>=tau
%logarithmic - otherwice
  
  tau= -0.5;  % position of the joint

  d  = - (1+tau)^2;
  e  = 1+2*tau;
  g  = 1/(1+tau);
  f  = tau+0.5*tau^2;
   
  t= (1./penpar).*fconstr;

  phi =zeros(size(t));
  dphi=zeros(size(t));
  d2phi=zeros(size(t));


  ind1=find(t>=tau);    %%% Quadratic branch
  if(length(ind1)>0),
    t1=t(ind1);
    phi(ind1) = 0.5*t1.^2+t1;
    dphi(ind1)= t1+1;
    d2phi(ind1)=1;
  end

  ind2=find(t<tau);     %%% Logarithmic branch
  if(length(ind2)>0),
    t2=t(ind2);
    phi(ind2) = d*log(g*(e-t2))+f;
    dphi(ind2)= d./(t2-e);
    d2phi(ind2)= -d./(t2-e).^2;
  end


%  phi= (t>=tau) .* (0.5*t.^2+t)  + (t<tau).*(d*log(g*(e-t))+f);
%  dphi=  (t>=tau) .*(t+1)  + (t<tau).*(d./(t-e));

  
  phi  = penpar.*multipliers.*phi;
  dphi = multipliers.*dphi;
  d2phi = 1./penpar.*multipliers.*d2phi;
  
%keyboard

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% TEST
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

a=5;
da=0.01;
f=[-a:da:a]';
n=length(f);

u=3*ones(n,1);
p=0.7;
[phi,dphi,d2phi]= penfun(f,p,u);
figure;plot(f,[phi,dphi,d2phi]);grid
figure;plot(f(1:n-1),[(dphi(2:n)-dphi(1:n-1))/da d2phi(1:n-1)]);grid
figure;plot(f(1:n-1),[d2phi(1:n-1)]);grid
figure;plot(f(1:n-1),[dphi(1:n-1)]);grid
figure;plot(f(1:n-1),(phi(2:n)-phi(1:n-1))/da);grid
figure;plot(f(1:n-1),(dphi(2:n)-dphi(1:n-1))/da);grid

