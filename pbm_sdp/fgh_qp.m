function [dphi,fobj,fconstr,F_aggr,grd_aggr,Hess_aggr]=fgh_qp(x,u,penpar,probl)
% function, grad & Hess of penalty aggregate
% for quadratic programming problem
%
%  min 1/2 x'Qx + c'x
%  s.t. Ax-b <= 0
%
%  matrix Q - symmetric positive semidefinite

Q=probl.Q;
c=probl.c;
A=probl.A;
b=probl.b;

Qx=Q*x;
%keyboard
fobj=0.5*x'*Qx+c'*x;
fconstr=A*x-b;

[phi,dphi,d2phi]= penfun(fconstr,penpar,u); % penalty fun. and its derivatives

F_aggr=fobj+sum(phi);

if nargout>4,  %%%%%%%%%%%%% Compute gradient %%%%%%%%%%%%%

  grd_obj= Qx+c;
  grd_aggr=grd_obj+A'*dphi;

end
if nargout>5,  %%%%%%%%%%%%% Compute Hessian %%%%%%%%%%%%%

  A1=mulmd(A',d2phi);  % A1=A'*diag(d2phi);

  Hess_aggr= Q + A1*A;

end
