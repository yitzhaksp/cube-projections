function [f,grd,Hess]=fgh_aggregate(x,penpar,u,probl),
% compute function, gradient and Hessian of the aggregate

global N_FUN_EVAL N_GRAD_EVAL N_HESS_EVAL GRAD_NORMS; 

  if  nargout==1,  % compute functions only

    [dphi,fobj,fconstr,f]=feval(probl.fungrad, x,u,penpar,probl);
    N_FUN_EVAL=N_FUN_EVAL+1;

  else
	  if nargout==2,  % compute functions and gradient
		  
		  [dphi,fobj,fconstr,f,grd]=feval(probl.fungrad, x,u,penpar,probl);

	  elseif nargout==3 		 % compute functions gradient and hessian
		  
		  %n=length(x);
		  %[dphi,fobj,fconstr,f,grd,Hess]=feval(probl.fungrad, x,u,penpar,probl,speye(n));
		  [dphi,fobj,fconstr,f,grd,Hess]=feval(probl.fungrad, x,u,penpar,probl);
		  N_HESS_EVAL=N_HESS_EVAL+1;
	  else
		  error('fgh_aggregate: wrong number of output arguments %d !!!\n',nargout);
	  end
	  N_GRAD_EVAL=N_GRAD_EVAL+1;
	  GRAD_NORMS=[GRAD_NORMS norm(grd)];
  end	  
