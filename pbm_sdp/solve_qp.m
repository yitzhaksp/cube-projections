%solve_qp: Generate quadratic programming problem and solve it  with PBM
%
%  min 1/2 x'Qx + c'x
%  s.t. Ax-b <= 0
%
%  matrix Q - symmetric positive semidefinite

% Michael Zibulevsky 2002 - 10.02.2009

addpath(genpath('.'))


probl.name= 'qp';
probl.fungrad = @fgh_qp; % User function for computing function, grad & Hess of
                         % the penalty aggregate


						  
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%     Generate data Q,c,A,b for quadratic programming problem
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

						  
% Create an arbitrary matrix:

R = [0.9501    0.4860    0.4565
     0.2311    0.8913    0.0185
     0.6068    0.7621    0.8214];

Q=R'*R; % This provides symmetry and  positive semidefinitess of Q

c=[ 2 3 4 ]';

A= [ 0.7919    0.7382    0.4057
     0.9218    0.1763    0.9355];

b=  [0.2  2.0335]';

[m,n]=size(A); % number of constraints and variables;


probl.Q=Q;
probl.c=c;
probl.A=A;
probl.b=b;


%=========================================================================
%         SET  PARAMETERS  of  OPTIMIZATION ALGORITHM
%=========================================================================

options=pbm_optionset(); % Set standard parameters of  PBM algorithm

options.NouterTol = 20;   % max number of outer PBM iterations
options.N_ucTol   = 300;  % max number of iter. for_ unconstr.opt.
options.ucDXtol   = 1e-12; % accuracy of  unconstr.opt. in x
options.ucDFtol   = 1e-12; % accuracy unconstr.opt. in f
options.normGrdTol= 1e-8; % for_ Newton method

options.p_tol=1e-4;         % minimal penalty parameter
options.DFtol=1e-20;        % stopping crireria for PBM
options.INFEAStol=1e-20;    % change in the objectiive and infeasibility


%%%Unconstrained optimization tool to use :
options.ucTool='newton_';  % For medium-size problems, when Hessian is available
%options.ucTool='minfunc';  % For medium and large problems: L-BFGS minFunc of Mark Schmidt, http://www.cs.ubc.ca/~schmidtm
%options.ucTool='fminunc';   % BFGS from matlab optimization toolbox
%options.ucTool='myfminu';   % old MATLAB toolbox unconstrained minimization (with Mzib modification)
options.ucTool='sesop__';
%options.ucTool ='LBFGS_C';   % L-BFGS in C by Naoaki Okazaki

options.NLP=1;    % 1 - PBM for NLP (nonlilnear programming)
options.SDP=0;    % 1 - PBM for SDP (nonlilnear semidefinite programming)
options.SUMMAX=0; % 1 - smoothing method of multipliers for summax problem



probl.optpar=options;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%      Solve the optimization problem with PBM method
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

x0=zeros(n,1);  % Initiate variables
u0=ones(m,1);   % Initiate Lagrange multipliers 

[x,u,probl]=pbmsdp(x0,u0,probl); % Call solver







