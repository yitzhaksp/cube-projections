function [dphi,f_aggr,g_aggr,H_aggr]=fgh(x,penpar,multipliers,probl_data),
%Compute value, gradient and hessian of penalty aggregate for NLP problem
%
%          min fobj(x) 
%   subject to fconstr_i(x)<=0, i=1,..,m
  
global N_FUN_EVAL N_GRAD_EVAL N_HESS_EVAL GRAD_NORMS; 
  
  if nargout<=2,  % compute functions only
    N_FUN_EVAL=N_FUN_EVAL+1;
    [dphi,fobj,fconstr,f_aggr]=feval(probl_data.fungrad,...
				     x,multipliers,penpar,probl_data);

    %fprintf('f');
  else
	  if nargout == 3          % compute functions and gradient
		  N_GRAD_EVAL=N_GRAD_EVAL+1;
		  [dphi,fobj,fconstr,f_aggr,g_aggr]=feval(probl_data.fungrad,...
			  x,multipliers,penpar,probl_data);
	  else                   % compute functions, gradient and Hessian
		  N_HESS_EVAL=N_HESS_EVAL+1;
		  [dphi,fobj,fconstr,f_aggr,g_aggr,H_aggr]=feval(probl_data.fungrad,...
			  x,multipliers,penpar,probl_data);
	  end
	  GRAD_NORMS=[GRAD_NORMS, norm(g_aggr)]; 

  %fprintf('g%.0e ',norm(g_aggr));
  %fprintf('.');
  end
return;



%%%%%%% GARBADGE: %%%%%%%%%%%%%%%%%%%%%%%




  %[fobj,fconstr]=feval(probl_data.obj_constr,x,probl_data);
  %[fobj,fconstr]=fc_problem(x,probl_data);

  [phi,dphi]= penfun(fconstr,penpar,multipliers);

  f_aggr=fobj+ sum(phi(:));

  if nargout>2,
    %g_aggr = feval(probl_data.gradlagr,x,dphi,probl_data);
    %g_aggr = gradlagr(x,dphi,probl_data);
  end

  if nargout>3,
    error('fgh: no hessian implimenteted yet!');
  end


