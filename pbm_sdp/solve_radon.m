%profile on
clear all;
close all;
addpath(genpath('../Functions'));
addpath(genpath('.'));
n=100;  m=n*n*n;
set_parameters_1; set_parameters_2;
read_image;
set_f_obj;
dimsA=[n,n,n]; dimsB=[3,n,n]; dimsb1=[n*n,1];
A=zeros(n,n,n);
A(1,:,:)=B1; A(:,1,:)=B2; A(:,:,1)=B3;
gen_right_side;
probl.b=b; probl.n=n;
probl.optpar=options; probl.penparw=penparw;

for k=1:nsim
        gen_x_start;
        u0=3e-4*ones(2*m,1);   % Initiate Lagrange multipliers 
        y0=f_obj(x0,b,n);
        [x_opt,u,probl]=pbmsdp1(x0,u0,probl); % Call solver
        y_opt(k)=f_obj(x_opt,b,n);
        xbin1=to_bin(x_opt); ybin1=f_obj(xbin1,b,n);
        xbin2=to_bin2(x_opt); ybin2=f_obj(xbin2,b,n);
        compute_errors;
        y_opt=y_opt'; err_bin=err_bin'; err_box=err_box';
end

% profile viewer
