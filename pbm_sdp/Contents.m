% PBM- penalty/barrier multiplier algorithm for nonlinear optimization
%       with functional constraints:
%
%          min fobj(x) 
%   subject to fconstr_i(x)<=0, i=1,..,nconstr
%                                            M.Zibulevsky
%                                            Version:13.11.2001; 09.02.2009
%
% Files:
%
% pbm           - PBM optimization solver
% pbm_optionset - set parameters of  PBM solver
%
% EXAMPLE of use: quadratic programming
%   solve_qp  - main script
%   fgh_qp    - compute function, gradient and hessian of the penalty aggregate
%   qp_print  - (empty) user function for printing results
%
%
% Auxiliary functions:
%
% cublinsrch1   - cubic safeguarded linesearch
% cholsub       - Cholesky substitution
% mulmd         - multiply matrix by diagonal matrix
%
% fgh,           - interface functions calling user provided function 
% fgh_aggregate,   for  computing function, gradient and hessian
% fun_aggregate, 
% grad_aggregate 
%                    
% Subdirectories:
%  
%  minFunc -  Limited memory BFGS  of Mark Schmidt with some updates (minFunc_mz)
%  bfgs_mz -  several BFGS quasi-Newton codes
