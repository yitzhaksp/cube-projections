function [VSDSV,fobj,AA,F_aggr,grd_aggr,HZ]=fgh_sdp(x,u,penpar,probl)
% function, grad & Hess of penalty aggregate
% for semidefinite programming problem
%
%  min  fobj(x)
%  s.t. matrices Ai(x) negative semidefinite
%

[fobj, gobj]=probl.fg_obj(x,probl);

F_aggr=fobj;

AA=probl.eval_A(x,probl); % cell array of matrices Ai(x)

VSDSV=AA; S=AA; lam=AA; VS=AA; % reserve space


mconstr=length(AA);

for i=1:mconstr
   
   V=u.V{i};
   B=V*AA{i}*V;
   
   [S{i},lam{i}]=eig(B);lam{i}=real(diag(lam{i})); S{i}=real(S{i});
   
   if nargout>5
	  nh=size(probl.Z,2);
	  [phi,dphi,d2phi]=penfun(lam{i},penpar,1); % penalty fun. and its derivatives
   else
	  [phi,dphi]           = penfun(lam{i},penpar,1); % penalty fun. and its derivatives
   end
   
   
   F_aggr=F_aggr+sum(phi);
   
   if nargout>4,  %%%%%%%%%%%%% prepare to compute gradient %%%%%%%%%%%%%
	  VS{i}=V*S{i};
	  VSDSV{i}=VS{i}*diag(dphi)*VS{i}';
   end
   
%    if nargout>5,  %%%%%%%%%%%%% Compute Hessian (not implemented yet) %%%%%%%%%%%%%
% 	
% 	  m=size(AA{i},1);
% 	  
% 	  P=dphi*ones(1,m);P=P-P';
% 	  Q=lam{i}*ones(1,m);Q=Q-Q';
% 	  ind=find(Q.^2<1e-17); Q(ind)=1;
% 	  Q=P./Q;
% 	   [ii,jj] = ind2sub(3,ind);
% 	   Q(ind)=d2phi(ii);
% 	   
	
   
   
end
   
if nargout>4,  %%%%%%%%%%%%% Compute gradient %%%%%%%%%%%%%
   grd_aggr=gobj+ probl.mult_adj_jacob_A(VSDSV,probl);
end
  
if nargout>5,  %%%%%%%%%%%%% Compute Hessian *Z  %%%%%%%%%%%%%
  HZ=0*probl.Z; %reserve space
  for j=1:nh
	  AA1=probl.mult_jacob_A(probl.Z(:,j),probl);
	  tmp=AA1; %reserve space
	  for i=1:mconstr
		 V=u.V{i};
		 [phi,dphi,d2phi]=penfun(lam{i},penpar,1); % penalty fun. and its derivatives
		 m=size(AA{i},1);
		 P=dphi*ones(1,m);P=P-P';
		 Q1=lam{i}*ones(1,m);Q1=Q1-Q1';
		 ind=find(Q1.^2<1e-17);
		 Q=P./(Q1+1e-50);
		 [ii,jj] = ind2sub([m,m],ind);
		 Q(ind)=d2phi(ii);
		
		 %tmp{i}=V*S{i}*(Q.*(S{i}'*V*AA1{i}*V*S{i}))*S{i}'*V;
		 tmp{i}=VS{i}*(Q.*(VS{i}'*AA1{i}*VS{i}))*VS{i}';
	  end
	  HZ(:,j)=probl.mult_adj_jacob_A(tmp,probl);
   end
end
	  

   
   
   
   
end


