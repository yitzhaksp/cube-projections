clear all
%load('data/Cube0.dat');
load('../../Projects_C/Radon_C/x_opt_c.dat');
PictureMaxValueOut=2.0;
n=10;
x_opt=x_opt_c;
x_opt_3d=reshape(x_opt,[n,n,n]);
Projection1=pr1(x_opt_3d);
Projection2=pr2(x_opt_3d);
Projection3=pr3(x_opt_3d);
Projection1=min(Projection1,PictureMaxValueOut);
Projection2=min(Projection2,PictureMaxValueOut);
Projection3=min(Projection3,PictureMaxValueOut);
figure;imagesc(Projection1);colormap(gray);colorbar;
figure;imagesc(Projection2);colormap(gray);colorbar;
figure;imagesc(Projection3);colormap(gray);colorbar;