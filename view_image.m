clear C;
addpath(genpath('Functions'));
showoriginals=true;
writeim=false;
scale=1; %1=linear, 2=log
pics=read_image(pars1.picpaths,pars1.n,pars1.img_maxvalue);
%binxs=convert_to_binary(x_opt);
A=reshape(x_opt, [pars1.n,pars1.n,pars1.n]);
A1=pr1(A); A2=pr2(A); A3=pr3(A);
AA1=bound_tozero(A1); AA2=bound_tozero(A2); AA3=bound_tozero(A3);
C{1}=AA1;  C{2}=AA2;  C{3}=AA3; 
if (showoriginals)
    C{4}=pics.B1; C{5}=pics.B2; C{6}=pics.B3;
end
viewimage1(C,scale);
if (writeim)
    imwrite(AA1,'house.gif','gif');
    imwrite(AA2,'boat.gif','gif');
    imwrite(AA3,'barbara.gif','gif');
end