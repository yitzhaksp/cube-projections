close all;
addpath(genpath('../useful_functions'));
addpath(genpath('.'));

set_parameters_1; set_parameters_2;
n=pars1.n; 
n2=n*n; n3=n*n*n;
% size of data must be n
load('data/picture1.dat');
load('data/picture2.dat');
load('data/picture3.dat');
load('data/Cube0.dat');
x0=Cube0;
RightSide=[picture1;picture2;picture3];
PictureMaxValueOut=max([ max(picture1) , max(picture2) , max(picture3)]);
[f_obj,probl.fungrad]=set_f_obj(pars1,RightSide);
probl.b=RightSide; probl.n=n;
probl.optpar=options; probl.penparw=penparw;
u0=1e-2*ones(2*n3,1);   % Initiate Lagrange multipliers 
stat0=compute_statistics1(x0,pars1);
[x_opt,u,probl,res]=pbmsdp1(x0,u0,probl); % Call solver
csvwrite('data/x_opt_matl.dat',x_opt) ;
stat=compute_statistics1(x_opt,pars1);
x_opt_3d=reshape(x_opt,[n,n,n]);
Projection1=pr1(x_opt_3d);
Projection2=pr2(x_opt_3d);
Projection3=pr3(x_opt_3d);
Projection1=min(Projection1,PictureMaxValueOut);
Projection2=min(Projection2,PictureMaxValueOut);
Projection3=min(Projection3,PictureMaxValueOut);
figure;imagesc(Projection1);colormap(gray);colorbar;
figure;imagesc(Projection2);colormap(gray);colorbar;
figure;imagesc(Projection3);colormap(gray);colorbar;
clear u u0;