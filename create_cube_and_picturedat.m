clear all;
addpath(genpath('.'));
set_parameters_1;
n=pars1.n; n2=n*n;n3=n*n*n;
PictureMaxValueOut=pars1.img_maxvalue;
picture1_orig=imread('data/imgs/originals/house.png');
picture2_orig=imread('data/imgs/originals/barbara.png');
picture3_orig=imread('data/imgs/originals/boat.png');
picture1=imresize(picture1_orig,[n,n]);
picture2=imresize(picture2_orig,[n,n]);
picture3=imresize(picture3_orig,[n,n]);
clear picture1_orig picture2_orig picture3_orig
picture1=double(picture1);
picture2=double(picture2);
picture3=double(picture3);
Picture1MaxValueIn=max(max( picture1 ));
Picture2MaxValueIn=max(max( picture2 ));
Picture3MaxValueIn=max(max( picture3 ));
picture1=(PictureMaxValueOut/Picture1MaxValueIn)*picture1;
picture2=PictureMaxValueOut/Picture2MaxValueIn*picture2;
picture3=PictureMaxValueOut/Picture3MaxValueIn*picture3;
picture1_v=reshape(picture1,[n2,1]);
picture2_v=reshape(picture2,[n2,1]);
picture3_v=reshape(picture3,[n2,1]);
csvwrite('data/picture1.dat',picture1_v);
csvwrite('data/picture2.dat',picture2_v);
csvwrite('data/picture3.dat',picture3_v);

Cube=zeros(n,n,n);
Cube(1,:,:)=picture1;
Cube(:,1,:)=picture2;
Cube(:,:,1)=picture3;
Cube_v=reshape(Cube,[n3,1]);
csvwrite('data/Cube0.dat',Cube_v);

%Cube1=rand(n3,1);
%csvwrite('data/Cube1.dat',Cube1);

Projection1=pr1(Cube);
Projection2=pr2(Cube);
Projection3=pr3(Cube);
Projection1=min(Projection1,PictureMaxValueOut);
Projection2=min(Projection2,PictureMaxValueOut);
Projection3=min(Projection3,PictureMaxValueOut);

figure;imagesc(Projection1);colormap(gray);colorbar;
figure;imagesc(Projection2);colormap(gray);colorbar;
figure;imagesc(Projection3);colormap(gray);colorbar;
